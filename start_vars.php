<?php
	date_default_timezone_set('America/Sao_Paulo');
	$data=date('Y-m-d H:i',time());
	//$data="2022-07-22 00:02";
	include_once("conn.php");
	$qryLista = mysqli_query($con, "SELECT * FROM calendario order by ano, semestre;") or die(mysqli_error($con));    
	while($resultado = mysqli_fetch_assoc($qryLista)){
		$vetor[] = $resultado; 
	}
	if (strtotime($data)<strtotime($vetor[0]['ini_sem'])){
		$semestre=0;
		$ano=0;
	} else if (strtotime($data)>strtotime(end($vetor)['fim_sem'])){
		$semestre=0;
		$ano=0;
	} else {
		$i=0;
		foreach ($vetor as $periodo){
			if (strtotime($data)<=strtotime($periodo['fim_sem'])) {
				break;
			}
			$i++;
		}
	}
	//echo ("Hoje: " . $data . " - Fim Semestre: " . $vetor[$i]['fim_sem'] . "<br>");
	return array(
		'codCalend'=>$vetor[$i]['cod_calend'],
		'anoMatr'=>$vetor[$i]['ano'],
		'semMatr'=>$vetor[$i]['semestre'],
		'iniAe'=>$vetor[$i]['ini_ae'],
		'fimAe'=>$vetor[$i]['fim_ae'],
		'iniAj'=>$vetor[$i]['ini_aj'],
		'fimAj'=>$vetor[$i]['fim_aj'],
		'iniEq'=>$vetor[$i]['ini_eq'],
		'fimEq'=>$vetor[$i]['fim_eq']
		);
?>