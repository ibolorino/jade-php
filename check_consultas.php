<?php 
	session_start();
	
	date_default_timezone_set('America/Sao_Paulo');
	include_once("conn.php");
	$param=include('../start_vars.php');
	require_once("xlsxwriter.class.php");
	switch($_POST["page"]) {
		
		case 1: //checar matriculas (servidor)
			$vetor = array();
			$q_aluno='';
			$q_curso_aluno='';
			$q_disc='';
			$q_curso_disc='';
			$q_depto='';
			$q_tipo='';
			$where='';
			if (!empty($_POST['aluno'])) $q_aluno=" aluno.nome like '%".$_POST['aluno']."%' and";
			if (!empty($_POST['curso_aluno'])) $q_curso_aluno=" aluno.curso like'%".$_POST['curso_aluno']."%' and";
			if (!empty($_POST['disc'])) $q_disc=" (disciplina.nome like '%".$_POST['disc']."%' or disciplina.turma like '%".$_POST['disc']."%') and";
			if (!empty($_POST['curso_disc'])) $q_curso_disc=" (disciplina.cod_curso='".$_POST['curso_disc']."' or curso.nome like '%".$_POST['curso_disc']."%') and";
			if (!empty($_POST['depto_disc'])) $q_depto=" (disciplina.cod_depto='".$_POST['depto_disc']."' or depto.nome like '%".$_POST['depto_disc']."%') and";
			if (!empty($_POST['tipo'])) $q_tipo=" (modalidade.tipo like '%".$_POST['tipo']."%' or modalidade.cod_mod = '" .$_POST['tipo']. "') and";
			$where= $q_aluno . $q_curso_aluno . $q_disc . $q_curso_disc . $q_depto . $q_tipo;
			$_SESSION['depto']=(!empty($_POST['depto_disc'])) ? $_POST['depto_disc'] : "tdsDpts";
			$_SESSION['tipo']=(!empty($_POST['tipo'])) ? $_POST['tipo'] : "tdsMatr";
			if (!empty($where)) {
				$where=' where'.$where;
				$where=substr($where,0,strlen($where)-4);
			}
			$querycons="select matricula.cod_matr as cod_matr, aluno.nome as aluno, aluno.curso as curso_aluno, modalidade.tipo as tipo, concat(disciplina.turma,' - ',disciplina.nome) as disc,curso.nome as curso_disc, matricula.status as status from matricula inner join aluno on matricula.cod_aluno = aluno.cod_aluno inner join disciplina on matricula.cod_disc=disciplina.cod_disc inner join curso on disciplina.cod_curso=curso.cod_curso inner join depto on disciplina.cod_depto = depto.cod_depto inner join modalidade on matricula.tipo=modalidade.cod_mod" .$where . " order by matricula.cod_matr;";
			$_SESSION["where"]=$where;
			$qryLista = mysqli_query($con, $querycons);    
			while($resultado = mysqli_fetch_assoc($qryLista)){
				$vetor[] = $resultado; 
			}
			echo json_encode($vetor);
			//echo json_encode($query);
			break;
		case 2: //checar matriculas (aluno)
			$vetor = array();
			$query="select aluno.nome as aluno, modalidade.tipo as tipo, concat(disciplina.turma,' - ', disciplina.nome) as disciplina, curso.nome as curso, matricula.status as status from matricula inner join aluno on matricula.cod_aluno=aluno.cod_aluno inner join disciplina on matricula.cod_disc=disciplina.cod_disc inner join curso on disciplina.cod_curso=curso.cod_curso inner join modalidade on matricula.tipo = modalidade.cod_mod where aluno.cpf='" .$_POST['cpf']. "' and matricula.ano=" .$param['anoMatr']." and matricula.semestre=". $param['semMatr']. ";";
			$qryLista = mysqli_query($con, $query);    
			while($resultado = mysqli_fetch_assoc($qryLista)){
				$vetor[] = $resultado; 
			}
			echo json_encode($vetor);
			break;
		case 3: //alterar status de matrícula
			$vetor=array();
			$query="update matricula set status='" .$_POST['status']. "' where cod_matr=" .$_POST['cod_matr'].";";
			$qryLista = mysqli_query($con, $query);    
			echo json_encode($query);
			break;
		case 4: //alterar em lote. Usar a variável where para pegar os registros e a linha de comando  update matricula inner join aluno on matricula.cod_aluno=aluno.cod_aluno set matricula.status=STATUS where $where;
			$vetor=array();
			$query="update matricula inner join aluno on matricula.cod_aluno = aluno.cod_aluno inner join disciplina on matricula.cod_disc=disciplina.cod_disc inner join curso on disciplina.cod_curso=curso.cod_curso inner join depto on disciplina.cod_depto = depto.cod_depto inner join modalidade on matricula.tipo=modalidade.cod_mod set matricula.status='" .$_POST['status']. "' " .$_SESSION["where"]. ";";
			$qryLista = mysqli_query($con, $query);  
			echo json_encode($_SESSION["where"]);
			break;
		case 6: //exporta a consulta para excel
			$filename=$_SESSION['depto'] ."-". $_SESSION['tipo'] .".xlsx";
			$query="select matricula.cod_matr as cod_matr, aluno.ra as ra, aluno.nome as aluno, aluno.curso as curso_aluno, aluno.universidade as universidade, aluno.cm as cm, aluno.cr as cr, modalidade.tipo as tipo, concat(disciplina.turma,' - ',disciplina.nome) as disc, matricula.justif as justificativa from matricula inner join aluno on matricula.cod_aluno = aluno.cod_aluno inner join disciplina on matricula.cod_disc=disciplina.cod_disc inner join depto on disciplina.cod_depto = depto.cod_depto inner join modalidade on matricula.tipo=modalidade.cod_mod" .$_SESSION['where'] . " order by matricula.cod_matr;";
			$_SESSION['filename']=$filename;
			$_SESSION['query']=$query;
			$result=array($filename,$query);
			echo json_encode($result);
			break;
	}
	

?>