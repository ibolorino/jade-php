<html>
	<head>
		<script src="../jquery-3.3.1.js" type="text/javascript"></script>
		<script src="../jquery.mask.js" type="text/javascript"></script>
		<script src="functions.js" type="text/javascript"></script>
		<link rel="stylesheet" type="text/css" href="estilo.css">
	</head>
	<body>
		<div class="width100 vis" id="divCalendario">
			<table id='tblCalendario' style='border-collapse: collapse;' frame="box" cellpadding="5">
				
			</table>
			<br>
		</div>
		
		<div class="width100 nvis" id="divEditCalendario">
			<table id='tblEditCalendario' style='border-collapse: collapse;' frame="box" cellpadding="8">
				
			</table>
			<br>
			
		</div>
		
		<div class="width100 nvis" id="divAddCalendario">
			<table id='tblAddCalendario' style='border-collapse: collapse;' frame="box" cellpadding="2">
				<tr>
					<td align="right">Ano: </td>
					<td><input type="text" id="txtAno" disabled></td>
					<td>Semestre: </td>
					<td><input type="text" id="txtSemestre" disabled></td>
				</tr>
				<tr><td colspan="4">&nbsp;</td></tr>
				<tr>
					<td align="right">Início Semestre: </td>
					<td><input type="text" id="txtIniSem" name="txtData"></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td align="right">Término Semestre: </td>
					<td><input type="text" id="txtFimSem" name="txtData"></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
				<tr>
					<td align="right">Início Aluno Especial: </td>
					<td><input type="text" id="txtIniAe" name="txtData"></td>
					<td>Horário: </td>
					<td><input type="text" name="txtHora"></td>
				</tr>
				<tr>
					<td align="right">Término Aluno Especial: </td>
					<td><input type="text" id="txtFimAe" name="txtData"></td>
					<td>Horário: </td>
					<td><input type="text" name="txtHora"></td>
				</tr>
				<tr>
					<td align="right">Início Aluno Ajustes: </td>
					<td><input type="text" id="txtIniAj" name="txtData"></td>
					<td>Horário: </td>
					<td><input type="text" name="txtHora"></td>
				</tr>
				<tr>
					<td align="right">Término Aluno Ajustes: </td>
					<td><input type="text" id="txtFimAj" name="txtData"></td>
					<td>Horário: </td>
					<td><input type="text" name="txtHora"></td>
				</tr>
				<tr>
					<td align="right">Início Aluno Equivalentes: </td>
					<td><input type="text" id="txtIniEq" name="txtData"></td>
					<td>Horário: </td>
					<td><input type="text" name="txtHora"></td>
				</tr>
				<tr>
					<td align="right">Término Aluno Equivalentes: </td>
					<td><input type="text" id="txtFimEq" name="txtData"></td>
					<td>Horário: </td>
					<td><input type="text" name="txtHora"></td>
				</tr>
				
			</table>
			<br>
		</div>
		
		
		<div class="nvis">
			<input type="button" id="btBuscar" value="Pesquisar">
			<input type="button" id="btTeste" value="Teste">
			<input type="button" id="btVoltar" value="Voltar" class="btPequeno">
			<input type="button" id="btIncluirCalendario" value="Incluir" class="btPequeno">
			<input type="button" id="btSalvarCalendario" value="Salvar" class="btPequeno">
		</div>
	
		<script>
			
			$(document).ready(function(){
				
				$.ajax({
					type: 'post',
					data: {page: 14},
					dataType: 'json',
					url: 'check.php',
					success: function(dtCalendario){
						for (var i=1;i<dtCalendario.length;i++){
							dtCalendario[i].Início=dateDia(dtCalendario[i].Início);
							dtCalendario[i].Término=dateDia(dtCalendario[i].Término);
						}
						$('#tblCalendario tr.remover').each(function(iFunc){
							$(this).remove();
						});
						$('#tblCalendario').append(exibeDadosTable(dtCalendario,1,Object.values(dtCalendario[0]).length,"zebraAzulTitulo", "zebraAzul"));
					},
					complete: function(dtCalendario){
						dtCalendario=dtCalendario.responseJSON;
						var icones=[];
						icones.push({'title':'Editar', 'id':'imgEditarCalendario', 'src':'images/edit-20.png', 'class':'imgLink'});
						addColumnTable($('#tblCalendario'),icones,'Editar','last');
						icones=[];
						icones.push({'title':'Excluir', 'id':'imgExcluirCalendario', 'src':'images/del-20.png', 'class':'imgLink'});
						addColumnTable($('#tblCalendario'),icones,'Excluir','last');
						
						$('#btIncluirCalendario').appendTo($('#divCalendario'));
						$('#btVoltar').appendTo($('#divCalendario'));
						$('#btVoltar').attr('name','voltarCalendario');
						
						$(document).on('click','#imgEditarCalendario', function(){
							var row = $(this).parent().parent().parent().children().index($(this).parent().parent());
							$.ajax({
								type: 'post',
								data: {page: 15, id: dtCalendario[row].ID},
								dataType: 'json',
								url: 'check.php',
								success: function(dtEditCalendario){
									$('#tblEditCalendario tr.remover').each(function(iFunc){
										$(this).remove();
									});
									$('#tblEditCalendario').append("<tr class='remover'><td colspan='3' align='center' style='font-weight: bold;'>" + dtEditCalendario[1]['Período'] + "</td></tr>");
									id='txtData';
									$('#tblEditCalendario').append(geraTableEdit(dtEditCalendario,id,2,Object.values(dtEditCalendario[0]).length-6));
									icones=[];
									icones.push({'type':'text', 'id':'txtHora', 'class':'borda', 'value':''});
									addColuna($('#tblEditCalendario'),'input', icones,'','last');
									$('#tblEditCalendario tr:nth-child(2) td:last').html('');
									$('#tblEditCalendario tr:nth-child(3) td:last').html('');
								},
								complete: function(dtEditCalendario){
									$('#tblEditCalendario tr:nth-child(1) td:last').remove();
									dtEditCalendario=dtEditCalendario.responseJSON;
									iData=1;
									$('#divEditCalendario #txtData').each(function(i){
										$(this).attr('id','txtData'+iData);
										$(this).attr('name','txtData');
										iData++
									});
									iHora=1;
									$('#divEditCalendario #txtHora').each(function(i){
										var campo='hora'+iHora;
										$(this).parent().prepend('Horário: ');
										$(this).attr('value',dtEditCalendario[1][campo]);
										$(this).attr('id','txtHora'+(iHora+2));
										$(this).attr('name','txtHora');
										iHora++
									});
									$('#divEditCalendario input[name=txtData]').mask('00/00/0000',{'placeholder':'dd/mm/aaaa'});
									$('#divEditCalendario input[name=txtHora]').mask('00:00',{'placeholder':'hh:mm'});
									$('#btSalvarCalendario').appendTo($('#divEditCalendario'));
									$('#btSalvarCalendario').attr('name','editCalendario');
									$('#btVoltar').appendTo($('#divEditCalendario'));
									$('#btVoltar').attr('name','divCalendario');
									//$('#btTeste').appendTo($('#divEditCalendario'));
									$('#divCalendario').removeClass('vis').addClass('nvis');
									$('#divEditCalendario').removeClass('nvis').addClass('vis');
									
									
									
								},
							})
						});
						
						
						
						
						
						
						$(document).on('click','#imgExcluirCalendario', function(){ //falta excluir no BD
							var row = $(this).parent().parent().parent().children().index($(this).parent().parent());
							dtCalendario[row].ID
							currVal=$('#tblCalendario tr:nth-child(' + (row + 1) + ') td:first').html();
							rx=/^(\d{1})(º semestre de )(\d{4})$/;
							hrArray=currVal.match(rx);
							//console.log(hrArray[1] + ' - ' + hrArray[3]);
							$.ajax({
								type: 'post',
								data: {page: 21, cod_calend: dtCalendario[row].ID, ano: hrArray[3], semestre: hrArray[1]},
								dataType: 'json',
								url: 'check.php',
								complete: function(dtDelCalendario){
									if (dtDelCalendario.responseJSON=='ok'){
										row=row+1;
										$('#tblCalendario').find('tr:nth-child(' + row + ')').remove();
									}
								},
							})
						});
						
					},
				});
			});
			
			$('#btSalvarCalendario').click(function(){
				if ($(this).attr('name')=='addCalendario'){
					$vazio=0
					$("input[name=txtData]").each(function(iFunc){
						if (!isDate($(this).val())){
							$vazio=1;
							$(this).addClass('vazio');
						} else {
							$(this).removeClass('vazio');
						}
					});
					$("input[name=txtHora]").each(function(iFunc){
						if (!isHour($(this).val())) {
							if ($(this).val('')){
								$(this).val('00:00');
							} else {
								$vazio=1;
								$(this).addClass('vazio');
							}
						} else {
							$(this).removeClass('vazio');
						}
					});
					if ($vazio==0){
						$.ajax({
							type: 'post',
							data: {
								page: 17, 
								ano: $('#txtAno').val(), semestre: $('#txtSemestre').val(), 
								ini_sem: convertDate($('#txtIniSem').val()+' 00:00',1), fim_sem: convertDate($('#txtFimSem').val() + ' 00:00',1), 
								ini_ae: convertDate($('#txtIniAe').val() + " " + $('#txtIniAe').parent().nextAll(':lt(2)').find('input[type=text]').val(),1), 
								fim_ae: convertDate($('#txtFimAe').val() + " " + $('#txtFimAe').parent().nextAll(':lt(2)').find('input[type=text]').val(),1), 
								ini_aj: convertDate($('#txtIniAj').val() + " " + $('#txtIniAj').parent().nextAll(':lt(2)').find('input[type=text]').val(),1), 
								fim_aj: convertDate($('#txtFimAj').val() + " " + $('#txtFimAj').parent().nextAll(':lt(2)').find('input[type=text]').val(),1),
								ini_eq: convertDate($('#txtIniEq').val() + " " + $('#txtIniEq').parent().nextAll(':lt(2)').find('input[type=text]').val(),1), 
								fim_eq: convertDate($('#txtFimEq').val() + " " + $('#txtFimEq').parent().nextAll(':lt(2)').find('input[type=text]').val(),1)
							},
							dataType: 'json',
							url: 'check.php',
							complete: function(dtAddCalendario){
								clearDiv($('#divAddCalendario'));
								$('#btVoltar').trigger('click');
								location.reload();
							},
						})
					}
				} else if ($(this).attr('name')=='editCalendario'){
					$vazio=0;
					$('#txtData').each(function(iFunc){
						if (!isDate($(this).val())){
							$vazio=1;
							$(this).addClass('vazio');
						} else {
							$(this).removeClass('vazio');
						}
					});
					$('#txtHora').each(function(iFunc){
						if (!isHour($(this).val())) {
							if ($(this).val('')){
								$(this).val('00:00');
							} else {
								$vazio=1;
								$(this).addClass('vazio');
							}
						} else {
							$(this).removeClass('vazio');
						}
					});
					if ($vazio==0){
						$.ajax({
							type: 'post',
							data: {
								page: 18, 
								ini_sem: convertDate($('#txtData1').val()+' 00:00',1), fim_sem: convertDate($('#txtData2').val() + ' 00:00',1), 
								ini_ae: convertDate($('#txtData3').val() + " " + $('#txtHora3').val(),1), 
								fim_ae: convertDate($('#txtData4').val() + " " + $('#txtHora4').val(),1), 
								ini_aj: convertDate($('#txtData5').val() + " " + $('#txtHora5').val(),1), 
								fim_aj: convertDate($('#txtData6').val() + " " + $('#txtHora6').val(),1),
								ini_eq: convertDate($('#txtData7').val() + " " + $('#txtHora7').val(),1), 
								fim_eq: convertDate($('#txtData8').val() + " " + $('#txtHora8').val(),1)
							},
							dataType: 'json',
							url: 'check.php',
							success: function(dtEditCalendario){
								clearDiv($('#divEditCalendario'));
								$('#btVoltar').trigger('click');
							},
						})
					}
				}
				
				
				
			});
			
			$('#btIncluirCalendario').click(function(){
				//$('#btTeste').appendTo($('#divAddCalendario'));
				$("input[name=txtData]").mask('00/00/0000',{'placeholder':'dd/mm/aaaa'});
				$("input[name=txtHora]").mask('QW:ER',{
					'placeholder':'hh:mm',
					translation: {
						'Q': {pattern: /[0-2]/},
						'W': {pattern: /[0-9]/},
						'E': {pattern: /[0-6]/},
						'R': {pattern: /[0-9]/},
					}
				});				
				$('#btSalvarCalendario').appendTo($('#divAddCalendario'));
				$('#btSalvarCalendario').attr('name','addCalendario');
				$('#btVoltar').appendTo($('#divAddCalendario'));
				$('#btVoltar').attr('name','divCalendario');
				
				$.ajax({
					type: 'post',
					data: {page: 16},
					dataType: 'json',
					url: 'check.php',
					success: function(dtAddCalendario){
						if (dtAddCalendario[0]['semestre']==2){
							$('#txtSemestre').val('1');
							$ano=parseInt(dtAddCalendario[0]['ano'])+1;
							$('#txtAno').val($ano);
						} else {
							$('#txtSemestre').val('2');
							$('#txtAno').val(dtAddCalendario[0]['ano']);
						}
					},
					complete: function(dtAddCalendario){
						dtAddCalendario=dtAddCalendario.responseJSON;
						console.log(dtAddCalendario);
					}
				});
				
				
				$('#divCalendario').removeClass('vis').addClass('nvis');
				$('#divAddCalendario').removeClass('nvis').addClass('vis');
			});
			
			
			$('#btVoltar').click(function(){
				$divAtual=$(this).parent().attr('id');
				$divDestino=$(this).attr('name');
				$(this).appendTo($("div[id=" + $divDestino + "]"));
				$("div[id=" + $divAtual + "]").removeClass('vis').addClass('nvis');
				$("div[id=" + $divDestino + "]").removeClass('nvis').addClass('vis');
			});
			
			$('#btTeste').click(function(){
				currVal=$('#tblEditCalendario tr:first td:first').html();
				rx=/^(\d{1})(º semestre de )(\d{4})$/;
				hrArray=currVal.match(rx);
				console.log(hrArray[1] + ' - ' + hrArray[3]);
				console.log(currVal.substr(0,1) + ' - ' + currVal.substr(currVal.length-4,4));
			});
			
		</script>

	</body>
</html>