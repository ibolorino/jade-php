<?php
	session_start();
?>
<html>
<head>
	<script src="jquery-3.3.1.js" type="text/javascript"></script>
	<script src="jquery.mask.js" type="text/javascript"></script>
	<script src="jquery.md5.js" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="estilo.css">
</head>
<body>
	

	<div id="divLogin" class="vis">
		<form id="frmLogin" method="post">
			Login: <input maxlength="4" type="text" id="txtLogin" class="borda"><label class="nvis"> Campo obrigatório</label><br>
			Senha: <input maxlength="20" type="password" id="txtSenha" class="borda"><label class="nvis"> Campo obrigatório</label>
		</form>
		<div id="divErroLogin" class='nvis'></div><p>
		<input type="button" id="btLogin" value="Conectar" class="btPequeno">
	</div>
	<p>&nbsp;</p>
	
	<div id='divFunc' class='nvis width100'>
		<table id='tblFora' width= '100%' style='border-collapse: collapse;'>
			<tr><td align='center' style='font-size: 20;'><b>Últimas Funcionalidades</b></td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td>
					<table id='tblFunc' width='100%' frame="box" style='border-collapse: collapse; border: 2px solid black;' cellpadding='2'>
						
					</table>
				</td>
			</tr>
		</table>
	</div>
	
	
	
	
	
	<script>
	
	
	
	$(document).ready(function(){
		$('#txtLogin').focus();
		$.ajax({
			type: 'post',
			data: {page: 11},
			dataType: 'json',
			url: 'check.php',
			success: function(dados){
				if (Object.values(dados).length>6){
					var nLinhas=5;
					$('#tblFora').after("<label id='lblMais' class='link'>Exibir mais...</label>");
				} else {
					var nLinhas=Object.values(dados).length-1;
				}					
				$("tr.remover").each(function(i){
					$(this).remove();
				});
				$('#tdAumentar').attr('colspan',dados[0].length);
				//$currentTr=$('#trDados');
				$inserir="<tr id='trDados' class='zebraAzulTitulo remover'>";
				$(dados[0]).each(function(i,header){
					$('#tdAumentar').attr('colspan',dados[0].length);
					$inserir+="<td style='font-weight: bold;' valing='top' class='bordaVertBlack bordaHorBlack'><b>" + header + "</b></td>";
				});
				$inserir+="</tr>";
				$('#tblFunc').append($inserir);
				$currentTr=$('#trDados');

					for (var i = 1; i<=nLinhas;i++){
						$inserir="<tr class='remover zebraAzul'>";
						for (var k = 0; k<Object.values(dados[i]).length;k++){
							$inserir+="<td  class='bordaVertBlack'>"+Object.values(dados[i])[k]+"</td>";
						}
						$inserir+="</tr>";
						$($inserir).insertAfter($currentTr);
						$currentTr=$currentTr.next();
					}
					//$inserir='<br>Mais de 5';
					//$('#divFunc table').append($inserir);
				$('td:first', $('#tblFunc tr:first')).attr('width','8%');
				$('td:last', $('#tblFunc tr:first')).attr('width','75%');
			},
			complete: function(dados){
				$('#lblMais').click(function(){
					var dados2 = dados.responseJSON;
					var nLinhas=Object.values(dados2).length-1;
					var maisLinhas=nLinhas-5;
					if ($(this).html()=='Exibir mais...'){//mostra mais
						$currentTr=$('#tblFunc tr:last');
						for (var i = 6; i<=nLinhas;i++){
						$inserir="<tr class='remover zebraAzul'>";
						for (var k = 0; k<Object.values(dados2[i]).length;k++){
							$inserir+="<td  class='bordaVertBlack'>"+Object.values(dados2[i])[k]+"</td>";
						}
						$inserir+="</tr>";
						$($inserir).insertAfter($currentTr);
						$currentTr=$currentTr.next();
					}
						$(this).html('Exibir menos...');
					} else { //mostra menos
						for (var k=1;k<=maisLinhas;k++){
							$('#tblFunc tr:last').remove();
						}
						$(this).html('Exibir mais...');
					}
					
					
				});
			},
		});
		$('#divFunc').removeClass('nvis').addClass('vis');
	});
	
	$('#btLogin').click(function(){
		
		if ($('#txtLogin').val()=='' || $('#txtSenha').val()==''){
			$('#divErroLogin').html('<b>Preencha todos os campos</b>');
			$('#divErroLogin').removeClass('nvis').addClass('vis');
		} else { //login e senha preenchidos, continua
			$('#divErroLogin').removeClass('vis').addClass('nvis');
			$.ajax({
				type: 'post',
				data: {user: $('#txtLogin').val(), senha: $('#txtSenha').val(), page: 1},	
				dataType: 'json',
				url: 'login.php',
				success: function(dados){
					if (dados=='senha'){ //senha incorreta
						alert('Senha incorreta');
						$('#txtSenha').val('');
						$('#txtSenha').focus();
					} else if (dados=='user') { //user não encontrado
						alert('Usuário não encontrado');
						$('#txtLogin').val('');
						$('#txtLogin').focus();
					} else { //logar
					
						$("#frmLogin").attr('action','matriculas.php');
						$("#frmLogin").submit();
					}
				},
			});
		}
	});
	
	$("#txtLogin").keyup(function (e) {
		if (e.which == 13) {
			$('#txtSenha').focus();
		}
	});
	
	$("#txtSenha").keyup(function (e) {
		if (e.which == 13) {
			$('#btLogin').trigger('click');
		}
	});

			
		
	</script>
</body>
</html>