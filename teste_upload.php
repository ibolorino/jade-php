<html>
	<head lang="en">
		<title>Teste Upload</title>
		<script src="jquery-3.3.1.js" type="text/javascript"></script>
		<script src="jquery.mask.js" type="text/javascript"></script>
		<link rel="stylesheet" type="text/css" href="estilo.css">
		<link rel="shortcut icon" href="">
		<meta charset="utf-8">
	</head>
	<body>
		<form id="frmUpload" action='upload.php' method="post" enctype="multipart/form-data">
			<label for='upFile' id='lbFile'>Selecione um arquivo</label>
			<input id="upFile" name="upFile" type="file">&emsp;<input type="text" id="txtFile"><p>
			<input id='btFile' class="btPequeno" type="button" value="Upload">
			
		</form>

		
	<script>
	
		var form_data = new FormData();
		
		$('#upFile').change(function(){
			var value =$(this).val();
			value=value.split('\\');
			$('#txtFile').val(value[value.length-1]);
			var file_data = $("#upFile").prop("files")[0];   
			form_data.append("arquivos[]", file_data);
		});
		
		$("#btFile").on("click", function() {
				if ($('#upFile').val()=="") {
					alert('Nenhum arquivo selecionado');
				} else {
					$.ajax({
						url: 'upload2.php', // point to server-side PHP script 
						dataType: 'text',  // what to expect back from the PHP script, if anything
						cache: false,
						contentType: false,
						processData: false,
						data: form_data,                         
						type: 'post',
						success: function(php_script_response){
							alert(php_script_response); // display response from the PHP script, if any
						}	
					});
				}
			});

		
		
	</script>
		
		
	</body>
</html>