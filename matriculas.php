<?php 
	session_start();
	date_default_timezone_set('America/Sao_Paulo');
	//modulo 1 = grad; modulo 2 = depto
	if (empty($_SESSION['user']) or ($_SESSION['modulo']!=2 and $_SESSION['modulo']!=1)) {
		header('Location: sistema.php');
	}
	$colSpan = $_SESSION['modulo']==2?'6':'7';
?>

<html>
	<head>
		<script src="jquery-3.3.1.js" type="text/javascript"></script>
		<script src="jquery.mask.js" type="text/javascript"></script>
		<script src="jquery-ui.js" type="text/javascript"></script>
		<script src="functions.js" type="text/javascript"></script>
		<link rel="stylesheet" type="text/css" href="estilo.css">
		<link rel="stylesheet" type="text/css" href="jquery-ui.css">
	</head>
	<body>
	<label id='lblMod' class='nvis'><?php echo $_SESSION['modulo']; ?></label>
	<div id="divSession" class="vis width100" style="text-align: right;">
		<form id='frmSair' method='post'>
			<b>Usuário: </b><label id='lblUser'><?php echo $_SESSION['user']; ?></label>&nbsp;&nbsp;&nbsp;<input type="button" class="btPequeno" id="btSair" value="Sair">
		</form>	
	</div>
	<?php
		//if ($_SESSION['modulo']==2){ //depto
	?>
	
	<div class="width100" id="divPesquisa">
		<table style='border-collapse: collapse; border: 2px solid #00A3DE;' frame="box" width="100%" cellpadding="2">
			<tr height='20'>
				<td rowspan='1' colspan='<?php echo ($colSpan) ?>' align ='left' valign ='bottom' style='font-weight: bold;font-style: italic;text-decoration: underline;color: #00437E;'>Parâmetros da pesquisa:</td>
			</tr>
			<tr height='20'>
				<td  valign ='bottom' style='color: rgb(0,0,0);' colspan='<?php echo ($colSpan) ?>'></td>
			</tr>
			<tr height='20'>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'>Aluno</td>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'>Curso do aluno</td>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'>Disciplina</td>
				<?php echo ($_SESSION['modulo']==2?'':"<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'>Departamento</td>") ?>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'>Curso da disciplina</td>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'>Tipo de Matrícula</td>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'>Status</td>
			</tr>
			<tr height='20'>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'><input type='text' id='aluno' style="width: 100%;" class="borda"></td>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'><input type='text' id='curso_aluno' style="width: 100%;" class="borda"></td>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'><input type='text' id='disc' style="width: 100%;" class="borda"></td>
				<?php echo ($_SESSION['modulo']==2?'':"<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'><input type='text' id='depto_disc' style='width: 100%;' class='borda'></td>") ?>
				
				
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'>
					<select id='curso_disc' style="width: 100%;" class="borda">
						<option value=""></option>
						<option value="72">Administração</option>
						<option value="45">Ciências Biológicas</option>
						<option value="18">Engenharia Agronômica</option>
						<option value="26">Medicina Veterinária</option>
						<option value="37">Zootecnia</option>
					</select>
				</td>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'>
					<select id='tipo' style="width: 100%;" class="borda">
						<option value=""></option>
						<option value="AE">Aluno Especial</option>
						<option value="AJ">Ajuste</option>
						<option value="EQ">Equivalentes</option>
					</select>
				</td>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'>
					<select id='selStatus' style="width: 100%;" class="borda">
					</select>
				</td>
			</tr>
			<tr>
				<td colspan='<?php echo ($colSpan) ?>'>&nbsp;</td>
			</tr>
			<tr>
				<td colspan='<?php echo ($colSpan) ?>'><input type="checkbox" id="chkAtend" value="1"> Exibir solicitações já atendidas pela Seção de Graduação</td>
			</tr>
			<tr>
				<td colspan='<?php echo ($colSpan) ?>'>&nbsp;</td>
			</tr>
			<tr>
				<td colspan='1'>
					<select id='selSemestre' style="width: 100%;" class="borda">
						
					</select>
				</td>
				<td colspan='<?php echo ($colSpan - 1) ?>'>&nbsp;</td>
			</tr>
			<tr>
				<td colspan='<?php echo ($colSpan) ?>'>&nbsp;</td>
			</tr>
			<tr>
				<td colspan='<?php echo ($colSpan) ?>'>
					<table border='0' width='100%' cellpadding='2' cellspacing='5' style='table-layout: fixed;'>
						<tr>
							<td>
								<b>Colunas a exibir:</b>
							</td>
						</tr>
						<tr style="font-size: 14;">
							<td id='tdCampos'>
								<input type='checkbox' id='aluno.ra' value='RA'><label> RA&emsp;</label>
								<input type='checkbox' id='aluno.rg' value='RG'><label> RG&emsp;</label>
								<input type='checkbox' id='aluno.cpf' value='CPF'><label> CPF&emsp;</label>
								<input type='checkbox' id='aluno.telefone' value='Telefone'><label> Telefone&emsp;</label>
								<input type='checkbox' id='aluno.email' value='Email'><label> Email&emsp;</label>
								<input type='checkbox' id='aluno.cp' value='Coef. Prog.'><label> Coef. de Progressão&emsp;</label>
								<input type='checkbox' id='aluno.cr' value='Coef. Rend.'><label> Coef. de Rendimento&emsp;</label>
								<input type='checkbox' id='aluno.curso' value='Curso'><label> Curso&emsp;</label>
								<input type='checkbox' id='aluno.universidade' value='Universidade'><label> Universidade&emsp;</label>
								<input type='checkbox' id='curso.nome' value='Curso da Disciplina'><label> Curso da Disciplina&emsp;</label>
								<input type='checkbox' id='depto.nome' value='Depto. Disc.'><label> Depto. da Disciplina&emsp;</label>
								<input type='checkbox' id='matricula2.parecer' value='Parecer'><label> Parecer&emsp;</label>
								<input type='checkbox' id='matricula2.justif' value='Justificativa'><label> Justificativa&emsp;</label>
								<input type='checkbox' id='matricula2.data' value='Data'><label> Data da Solicitação&emsp;</label>
								<?php echo ($_SESSION['modulo']==2?'':"<input type='checkbox' id='matricula2.obsgrad' value='Obs'><label> Observação&emsp;</label>") ?>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>

		
	</div>
	
	
	
	<div class="linhaestreito">
		<p>
		<input type="button" id="btBuscar" value="Pesquisar">

	</div>
	<div id="divBt" class="nvis">
		<p>
		<select id="selLote">
		</select>
		&nbsp;
		<input type="button" id="btChgStatus" value="Alterar todos os status" class="btPequeno">
	</div>
	<div id="divNotFound" class="nvis">
		<h3>Nenhuma solicitação de matrícula encontrada.</h3>
	</div>
	
	<div id="divXls" class="nvis">
		<form id="frmXls" method="post">
			<input type="text" id="txtFile" name="txtFile">
			<input type="text" id="txtQuery" name="txtQuery">
		</form>
	</div>
	<p>

	<div id="divResumo" style="width:20%;">
		<table id='tblResumo' width='100%' style='border-collapse: collapse; border: 2px solid black;' cellpadding="2" frame="box">
			<tr class='zebraAzulTitulo'><td colspan='2' class='borda' align="center"><b>Resumo: </td></tr>
			<tr class='zebraAzul'>
				<td class='bordaVertBlack'>Total de solicitações</td>
				<td class='bordaVertBlack' id='tdResTot'></td>
			</tr>
			<tr class='zebraAzul'>
				<td class='bordaVertBlack'>Solicitações analisadas</td>
				<td class='bordaVertBlack' id='tdResAn'></td>
			</tr>
			<tr class='zebraAzul'>
				<td class='bordaVertBlack'>Solicitações atendidas pela STG</td>
				<td class='bordaVertBlack' id='tdResAt'></td>
			</tr>
			<tr class='zebraAzul'>
				<td class='bordaVertBlack'><b>Solicitações pendentes</b></td>
				<td class='bordaVertBlack' id='tdResPen'></td>
			</tr>			
		</table>
	</div>	
		
	<input id='btResumo' type='button' class='nvis'>
	
	<div id="divShowDados" class="nvis width100">
			<table width= "100%" style="border-collapse: collapse;">
				<tr height="20">
					<td id="tdAumentar">
						<table width="100%" border="0">
							<tr>
								<td align="left">Matrículas:</td>
								<td id="nReg" align="right"></td>
							</tr>
						</table> 
					</td>
				</tr>
				<tr>
					<td>
						<table id='tblDados' width='100%' frame="box" style='border-collapse: collapse; border: 2px solid black; font-size: 14;' cellpadding='2'>
						
						</table>
					</td>
				</tr>			
			</table>
			<p>
			<table id='tblLegenda' width='100%' frame="box" style='border-collapse: collapse; border: 2px solid black;' cellpadding="2">
				<tr class='zebraAzulTitulo'><td colspan='2' class='borda' align="center"><b>Legenda: </td></tr>
				<tr class='zebraAzul'>
					<td class='bordaVertBlack'><i><u>Coef. Prog.</u></i></td>
					<td class='bordaVertBlack'>Coeficiente de Progressão. Relação entre carga horária integralizada pelo aluno e carga horária total do curso. Quanto mais próximo de 1, mais próximo está o aluno de concluir o curso.</td>
				</tr>
				<tr class='zebraAzul'>
					<td class='bordaVertBlack'><i><u>Coef. Rend.</u></i></td>
					<td class='bordaVertBlack'>Coeficiente de Rendimento. Média aritmética simples das notas, incluindo reprovações. </td>
				</tr>
				<tr class='zebraAzul'><td colspan='2' class='bordaVertBlack bordaHorBlack'><b>IMPORTANTE: Alunos que estão sem RA e Coeficientes são alunos de fora da FCAV.</td></tr>
			</table>
	</div>
	<p>



	</select>
	
	
	<script>
	
	$('#tdCampos label').click(function(){
		$(this).prev().prop('checked', !($(this).prev().prop('checked')));
	});


	
	
	
	$(document).ready(function(){
		loadSelect($('#selSemestre'), 'calendario', function(){loadResumo();});
		loadSelect($('#selStatus'), 'status', function(){return false;}, ' ');
		loadSelect($('#selLote'), 'status');
	});
	
	
	
	$('#btSair').click(function(){
		$.ajax({
			type: 'post',
			data: {page: 2},
			dataType: 'json',
			url: 'login.php',
			complete: function(){
				$("#frmSair").attr('action','sistema.php');
				$("#frmSair").submit();
			}
		});
	});
	
	
	function loadResumo(){
		$user='<?php echo($_SESSION['user']) ?>';
		//console.log ('semestre: ' + $('#selSemestre').val());
		$.ajax({
			type:'post',
			data:{user: $user, page: 12, calend: $('#selSemestre').val()},
			dataType:'json',
			url:'check.php',
			success: function(dados){
				//console.log(dados);
				$('#tdResTot').html(dados[0]);
				$('#tdResAn').html(dados[1]);
				$('#tdResPen').html(dados[2]);
				$('#tdResAt').html(dados[3]);
			},
		})
	}	
	
	
	
	
	
	$('#btTeste').click(function(){
		$objeto = 'calendario';
		loadSelect($('#selTeste'), $objeto,' ');		
	});
	
	
	
	$('#selSemestre').change(function(){
		loadResumo();
	});
	
	/*
	
	
	
	
	
	
	
	
	
	
	
	
	*/
	//Clique no botão buscar
	$("#btBuscar").click(function() {
		$atend='n';
		if ($('#chkAtend').prop('checked')) {
			$atend='s';
		}
		$user='<?php echo($_SESSION['user']) ?>';
		$depto=($user=='grad')?$('#depto_disc').val():$user;
		var camposExtras={};
		$('#tdCampos input[type=checkbox]:checked').each(function(){
			camposExtras[$(this).attr('id')]=$(this).val();
		});
		$data=camposExtras;
		camposExtras['page']=20;
		$.ajax({
			type:'post',
			data: camposExtras,
			dataType:'json',
			url:'check.php',
			complete: function(extras){
				
				extras=extras.responseJSON
				$.ajax({
					type:'post',
					data: {aluno: $('#aluno').val(), curso_aluno: $('#curso_aluno').val(), disc: $('#disc').val(), depto_disc: $depto, curso_disc: $('#curso_disc').val(), tipo: $('#tipo').val(), status: $('#selStatus').val(), atend: $atend, camposExtras: extras, page: 9, calend: $('#selSemestre').val()},
					dataType: 'json',
					url: 'check.php',
					success: function(dados){
						//console.log(dados);
						$iniIndex=dados[Object.values(dados).length-1];
						$nCol=Object.values(dados[0]).length;
						if (Object.values(dados).length>1){
							$("tr.remover").each(function(i){
								$(this).remove();
							});
							$("#nReg").empty();
							$("#nReg").append(dados.length-2 + " solicitações de matrícula encontradas. <input type='button' id='btTsv' value='Exportar' class='btPequeno'>");
							$inserir="<tr id='trDados' class='remover zebraAzulTitulo'>";
							$('#tdAumentar').attr('colspan',$nCol-1);
							for (var i=$iniIndex;i<$nCol;i++){	
								header=Object.values(dados[0])[i];  ///continuar para fazer a exibição dinamica
								$largura="";
								//if (header=="RA") $largura=" width='5%'";
								//if (header=="Aluno") $largura=" width='15%'";
								if (header=="Justificativa") $largura=" width='35%'";
								//if (header=="Disciplina") $largura=" width='20%'";
								if (header=="Status") {
									header+="<br><font size='1'>(<u>Clique para alterar</u>)</font>";
									$largura=" width='8%'";
								}
								$inserir+="<td class='bordaVertBlack bordaHorBlack' style='font-weight: bold;' align='center' valing='top'" + $largura + ">" + header + "</td>";
							};
							$inserir+="</tr>";
							$('#tblDados').append($inserir)
							$currentTr=$('#trDados');
							for (var i=1;i<Object.values(dados).length-1;i++){
								var $inserir="<tr class='remover zebraAzul'>";
								dados[i]['RA']=(dados[i]['RA']==null)?'':dados[i]['RA'];
								dados[i]['Coef. Matr.']=(dados[i]['Coef. Matr.']==null)?'':dados[i]['Coef. Matr.'];
								dados[i]['Coef. Rend.']=(dados[i]['Coef. Rend.']==null)?'':dados[i]['Coef. Rend.'];
								dados[i]['Coef. Prog.']=(dados[i]['Coef. Prog.']==null)?'':dados[i]['Coef. Prog.'];
								for (var j = $iniIndex;j<$nCol-2;j++){
									$inserir+="<td class='bordaVertBlack'>"+Object.values(dados[i])[j]+"</td>";
								}
								$disable=" class='nlink'";
								if ($user=='grad'){
									$disable=" class='link'"
								}else if (dados[i].Atendida=='não'){
									$disable=" class='link'"
								}
								$inserir+="<td class='bordaVertBlack'><label class='nvis'>"+i+"</label><label"+$disable+">"+Object.values(dados[i])[$nCol-2]+"</label>";
								$inserir += "<br><div class='nvis alter'>Salvo!</div></td>";
								
								if ($user=='grad'){
									if (Object.values(dados[i])[$nCol-1]=='não'){
										$inserir+="<td class='bordaVertBlack'><select class='selAtend'><option value='não' selected>Não</option><option value='sim'>Sim</option></select><br><div class='nvis alter'>Salvo!</div></td>";
									} else {
										$inserir+="<td class='bordaVertBlack'><select class='selAtend'><option value='não'>Não</option><option value='sim' selected>Sim</option></select><br><div class='nvis alter'>Salvo!</div></td>";
									}
									$inserir += "<br><div class='nvis alter'>Salvo!</div></td>";
								} else {
									$inserir+="<td class='bordaVertBlack'>"+Object.values(dados[i])[$nCol-1]+"</td></tr>";
								}
								$($inserir).insertAfter($currentTr);
								$currentTr=$currentTr.next();
							}
							$("#divShowDados").removeClass("nvis").addClass("vis");
							$("#divNotFound").removeClass("vis").addClass("nvis");
							$("#divBt").removeClass("nvis").addClass("vis linhalargo");
						} else { //nao encontrou
							$("#divNotFound").removeClass("nvis").addClass("vis");
							$("#divShowDados").removeClass("vis").addClass("nvis");
							$("#divBt").removeClass("vis linhalargo").addClass("nvis");
						}
					},
			
			
					complete: function(dados2){
						dados=dados2.responseJSON;
						
						
						//alterar atendida
						
						$('select.selAtend').change(function(){
							var $sel = $(this);
							$dadosIndex=$(this).parent().prev().find('label.nvis').html();
							$.ajax({
								type: 'post',
								data:{page: 6, cod_matr: dados[$dadosIndex].codMatr, atend: $(this).val()},
								dataType: 'json',
								url: 'check.php',
								complete: function(){
									//console.log($sel.next().next().removeClass('nvis').addClass('vis'));
								}
							});
							
						});
						
						
						
						
						
						//Clique no botão Alterar em lote
						$("#btChgStatus").click(function(){
							$.ajax({
								type:'post',
								data:{page: 10, status: $("#selLote").val()},
								dataType: 'json',
								url: 'check.php',
								success: function(dados){
									//console.log(dados);
									$("label.link").each(function(){
										$dadosIndex=$(this).parent().find('label.nvis').html();
										dados[$dadosIndex].Status=$("#selLote").val();
										$(this).text($("#selLote").val());
										$(this).parent().find('div').removeClass('nvis').addClass('vis');
									});
								},
							});
							//$('#btResumo').trigger('click');
							loadResumo();
						});
				
						$('label.link').click(function(){
							$cel=$(this).parent();
							$dadosIndex=$(this).parent().find('label.nvis').html();
							$htmlDialog="<div id='divDialog' class='nvis'>"
							$htmlDialog+=($user=='grad')?"Observação:<textarea id='txtObsDialog' class='obs' maxlength='220' style='width: 100%;'></textarea><p>":"";
							$htmlDialog+="Parecer do Docente:<textarea id='txtParecerDialog' class='obs' maxlength='220' style='width: 100%;'></textarea><p>Alterar Status:<select id='selStatusDialog' style='width: 100%;' class='borda'></select><p><div align='right' id='divBtDIalog'><input type='button' class='btPequeno' value='Salvar' id='btDialogSalvar'><input type='button' class='btPequeno' value='Cancelar' id='btDialogCancel'></div></div>";
							//<option value='Deferido'>Deferido</option><option value='Em análise'>Em análise</option><option value='Indeferido'>Indeferido</option>
							$('body').append($htmlDialog);
							loadSelect($('#selStatusDialog'), 'status', function(){$('#selStatusDialog').val(dados[$dadosIndex].Status);});
							$height=($user=='grad')?430:300;
							$('#divDialog').dialog({ 
								autoOpen: false,
								modal: true,
								width: 400,
								height: $height,
								title: "Parecer da Solicitação",
								position: { my: "center", at: "top", of: window },
								closeOnEscape: false,
								open: function(event, ui) {
									$(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
									$(".ui-dialog").find(".ui-widget-header").css("background", "#00A3DE");
									$(".ui-dialog").find(".ui-widget-header").css("color", "#FFF");
									$(".ui-dialog").css("border", "2px solid #00A3DE");
								}
							});
							//$('#selStatusDialog').val(dados[$dadosIndex].Status);
							$('#selStatusDialog').val('Em análise');
							$('#txtParecerDialog').val(dados[$dadosIndex].Parecer);
							if ($user=='grad') $('#txtObsDialog').val(dados[$dadosIndex].Obs);
							$('#divDialog').dialog('open');
							$('#btDialogCancel').click(function(){
								$('#divDialog').dialog('close');
								$('#divDialog').remove();
							});
							$('#btDialogSalvar').click(function(){
								$.ajax({
									type:'post',
									data: {cod_matr: dados[$dadosIndex].codMatr, status: $('#selStatusDialog').val(), parecer: $('#txtParecerDialog').val(), obsgrad: $('#txtObsDialog').val(), page: 8},
									dataType: 'json',
									url: 'check.php',
									success: function(dados3){
										dados[$dadosIndex].Status=dados3.status;
										dados[$dadosIndex].Parecer=dados3.parecer;
										dados[$dadosIndex].Obs=dados3.Obs;
										$cel.find('label.link').text($('#selStatusDialog').val());
										$cel.find("div").removeClass("nvis").addClass("vis");
									},
									complete: function(){
										//console.log($('#selStatusDialog').val());
										$('#btDialogCancel').trigger('click');
											//$('#btResumo').trigger('click');
											loadResumo();
									}
								});
							});
						});

						$("#btTsv").click(function(){
							$.ajax({
								type:'post',
								data: {page: 7},
								dataType: 'json',
								url: 'check.php',
								success: function(dtTsv){
									//console.log(dados);
									$("#txtFile").val(dtTsv[0]);
									$("#txtQuery").val(dtTsv[1]);
									$("#frmXls").attr('action','GeraXls.php');
									$("#frmXls").submit();
								},
							});
						});
					}
				});
			}
		});
	});
	
//alteração de status de matrícula
	
	
	</script>
	
	
	
	
	
	
	</body>
</html>