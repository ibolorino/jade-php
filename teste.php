<html>
	<head>
		<script src="../jquery-3.3.1.js" type="text/javascript"></script>
		<script src="../jquery.mask.js" type="text/javascript"></script>
		<script src="functions.js" type="text/javascript"></script>
		<link rel="stylesheet" type="text/css" href="estilo.css">
	</head>
	<body>
		<input type='button' value='teste' class='btPequeno' id='btTeste'><p>
<?php

session_start();
include 'excel_reader.php'; // include the class


// creates an object instance of the class, and read the excel file data
$excel = new PhpExcelReader;
$excel->read('alunos.xls');

// this function creates and returns a HTML table with excel rows and columns data
// Parameter - array with excel worksheet data
	function sheetData($sheet) {
		var $arrayTable=array();
		$re = "<table style='border-collapse: collapse;' border='1' id='tblDados'>"; // starts html table
		$x = 1;
		while($x <= $sheet['numRows']) {
			$re .= "<tr>\n";
			$y = 1;
			while($y <= $sheet['numCols']) {
				$cell = isset($sheet['cells'][$x][$y]) ? $sheet['cells'][$x][$y] : '';
				$cell= mb_convert_encoding($cell, 'utf-8', 'iso-8859-1');
				$re .= " <td>$cell</td>\n"; 
				$arrayTable[$x][$y]=$cell;
				$y++;
			} 
		$re .= "</tr>\n";
		$x++;
		}
		return $re .'</table>'; // ends and returns the html table
	}

	/*
$nr_sheets = count($excel->sheets); // gets the number of worksheets
$excel_data = ''; // to store the the html tables with data of each sheet

// traverses the number of sheets and sets html table with each sheet data in $excel_data
for($i=0; $i<$nr_sheets; $i++) {
 $excel_data .= sheetData($excel->sheets[$i]); 
}
*/
 $excel_data .= sheetData($excel->sheets[0]);
//echo $excel_data; // outputs HTML tables with excel file data


echo 'funcionando';
?>

<script>

	$(document).ready(function(){
		console.log("array: " + <?php echo ($arrayTable[1][1]);?>);
	});
	
	$('#btTeste').click(function(){
		//console.log(queryFromTable('aluno_teste',$('#tblDados'),false,true));
		$fields=['cod_aluno','ra','nome','rg','cpf','curso','cm','cp','cr','mp','ativo','universidade'];
		$valueExtra='"s","FCAV"';
		$.ajax({
			type:'post',
			data:{page: 100, qry: queryFromTable('aluno_teste',$fields, $('#tblDados'),false,true,$valueExtra)},
			dataType: 'json',
			url: 'check.php',
			success: function(dados){
				console.log(dados);
			},
		});
	});
	
	$('#tblDados td').dblclick(function(){
		$texto=$(this).html();
		$(this).html('');
		$(this).append("<input type='text' value='" + $texto + "' id='txtEdit'>");
		console.log($(this).html());
	});
	
	$(document).on('blur','#txtEdit', function(){
		$texto=$(this).val();
		$(this).parent().html($texto);
	});
	
</script>
</body>
</html>