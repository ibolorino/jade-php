<?php 
	session_id('sessionMatricula');
	session_start();
	date_default_timezone_set('America/Sao_Paulo');
	include_once("conn.php");
	$calend=include('start_vars.php');
	$vetor = array();
	switch($_POST["page"]) {
	
		case 1: //pagina inicio*/ OK
			
			$dados = mysqli_query($con, "SELECT * FROM aluno where cpf ='" .$_POST["cpf"]. "';");    
			$resultado = mysqli_fetch_assoc($dados);
			
			if ($resultado != Null){
				$_SESSION['cod_aluno']=$resultado['cod_aluno'];
				$_SESSION['ativo']=$resultado['ativo'];
			}
			echo json_encode($resultado);
			
			//echo json_encode($_SESSION['cod_aluno']);
			break;
		case 2: //pagina dados OK
			if ($_POST["cons"]=="insert"){
				$query="insert into aluno values (null,'','" .$_POST['nome']. "','" .$_POST['rg']. "','" .$_POST['cpf']. "','" .$_POST['univ']. "','" .$_POST['curso']. "','" .$_POST['tel']. "','" .$_POST['email']. "','','','','n','','','');";
				$qryLista = mysqli_query($con, $query) or die(mysqli_error($con));
				//echo json_encode("Inserido");
			} else if ($_POST["cons"]=="update"){
				$query="update aluno set telefone='" .$_POST['tel']. "', email='" .$_POST['email']. "' where cpf='" .$_POST['cpf']. "';" ;
				$qryLista = mysqli_query($con, $query) or die(mysqli_error($con));
				//echo json_encode("Atualizado");
			}
			$query="select * from aluno where cpf ='" .$_POST['cpf']. "';";
			$dados = mysqli_query($con, $query) or die(mysqli_error($con));
			$resultado = mysqli_fetch_assoc($dados);
			$_SESSION['cod_aluno']=$resultado['cod_aluno'];
			$_SESSION['ativo']=$resultado['ativo'];
			if (empty($resultado['ra'])){ //aluno de fora, pode fazer apenas aluno especial
				echo json_encode("fora");
				//echo json_encode($_SESSION['cod_aluno']);
			} else { //aluno da fcav, pode fazer todas as matriculas
				echo json_encode("fcav");
				//echo json_encode($_SESSION['cod_aluno']);
			}
			
			break;
		case 3: //pagina curso OK
			$data=date('Y/m/d - H:i',time());
			$query="select * from matricula2 where cod_aluno=" .$_SESSION['cod_aluno']. " and cod_turma=" .$_POST['cod_disc'] . ";";
			$qryLista = mysqli_query($con, $query);
			if (mysqli_num_rows($qryLista)>=1){
				echo json_encode("repetida");
				break;
			}
			if ($_SESSION['uploadOk']==false){
				echo json_encode('uploadNotOk');
				break;
			}
			$query="insert into matricula2 values (0, '" .$_POST['cod_disc']. "','" .$_SESSION['cod_aluno']. "','" .$_POST['tipo']. "','" .$data. "','" .$_POST["justif"]. "', 'Em análise','não','','');";
			$qryLista = mysqli_query($con, $query);
			$query="select * from matricula2 order by cod_matr desc limit 1;";
			$dados = mysqli_query($con, $query);
			$_SESSION['cod_matr']=mysqli_fetch_assoc($dados)['cod_matr'];
			$query="select aluno.nome as Nome, modalidade.tipo as Tipo, concat(disciplina2.cod_disc, turmas.turma,' - ',disciplina2.nome) as 'Disciplina',curso.nome as Curso, matricula2.justif as Justificativa, matricula2.data as Data from matricula2 inner join turmas on matricula2.cod_turma=turmas.cod_turma inner join aluno on matricula2.cod_aluno = aluno.cod_aluno inner join disciplina2 on turmas.cod_disc=disciplina2.cod_disc inner join curso on turmas.cod_curso=curso.cod_curso inner join modalidade on matricula2.tipo=modalidade.cod_mod where matricula2.cod_matr=".$_SESSION['cod_matr'].";";
			$dados = mysqli_query($con, $query) or die(mysqli_error($con));
			$vetor[] = mysqli_fetch_assoc($dados); 
			echo json_encode($vetor);
			break;
			
		case 4: //checar matriculas (aluno) OK
			$vetor = array();
			$_SESSION['qryConsAluno']="select matricula2.atendida, matricula2.cod_matr, aluno.nome as Aluno, modalidade.tipo as Tipo, concat(disciplina2.cod_disc, turmas.turma,' - ',disciplina2.nome) as Disciplina, curso.nome as Curso, matricula2.status as Status, matricula2.parecer as Parecer from matricula2 inner join turmas on matricula2.cod_turma=turmas.cod_turma inner join aluno on matricula2.cod_aluno = aluno.cod_aluno inner join disciplina2 on turmas.cod_disc=disciplina2.cod_disc inner join curso on turmas.cod_curso=curso.cod_curso inner join modalidade on matricula2.tipo=modalidade.cod_mod inner join calendario on (turmas.semestre = calendario.semestre and turmas.ano = calendario.ano) ";
			$_SESSION['whereConsAluno']=" aluno.cpf='" .$_POST['cpf']. "' and calendario.cod_calend=" .$calend['codCalend'];
			
			$query=$_SESSION['qryConsAluno'] . "where" . $_SESSION['whereConsAluno'] . " order by status;";
			//echo json_encode($query);
			//break;
			$qryLista = mysqli_query($con, $query);    
			
			
			$fields=mysqli_fetch_fields($qryLista);
			foreach ($fields as $val)
			{
				$header[]=$val->name;
			}
			$vetor[]=$header;
			while($resultado = mysqli_fetch_assoc($qryLista)){
				$vetor[] = $resultado; 
			}
			echo json_encode($vetor);
			break;	
		
		case 5: //alterar para matr_excluidas2
			$vetor=array();
			$data=date('Y/m/d - H:i',time());
			$query="insert into matr_excluidas2 select matricula2.*, '" .$data. "' from matricula2 where cod_matr=" .$_POST['cod_matr'].";";
			$qryLista=mysqli_query($con,$query);
			$query="delete from matricula2 where cod_matr=" .$_POST['cod_matr'].";";
			$qryLista=mysqli_query($con,$query);
			/*
			$query=$_SESSION['qryConsAluno']. "where" . $_SESSION['whereConsAluno'];
			$qryLista=mysqli_query($con,$query);
			while($resultado = mysqli_fetch_assoc($qryLista)){
				$vetor[] = $resultado; 
			}
			*/
			echo json_encode('ok');
			break;
			
		case 6: //alterar matricula atendida OK
			$vetor=array();
			$query="update matricula2 set atendida='" .$_POST['atend']. "' where cod_matr=" .$_POST['cod_matr'].";";
			$qryLista = mysqli_query($con, $query);    
			echo json_encode($query);
			break;		
			
				
		case 7: //exporta a consulta para excel OK
			$_SESSION['filename']="Consulta.xlsx";
			$result=array($_SESSION['filename'],$_SESSION['query']);
			echo json_encode($result);
			break;		
				
				
		case 8: //alterar status de matrícula OK
			$vetor=array();
			$query="update matricula2 set obsgrad='" .$_POST['obsgrad']. "', status='" .$_POST['status']. "', parecer='" .$_POST['parecer']. "' where cod_matr=" .$_POST['cod_matr'].";";
			$qryLista = mysqli_query($con, $query);  
			$vetor['status']=$_POST['status'];
			$vetor['parecer']=$_POST['parecer'];
			//$vetor={'status' => $_POST['status'], 'parecer' => $_POST['parecer']};
			echo json_encode($vetor);
			break;		
				
		case 9: //checar matriculas (depto) OK
			$vetor = array();
			$q_aluno='';
			$q_curso_aluno='';
			$q_disc='';
			$q_curso_disc='';
			$q_depto=($_SESSION['modulo']==2)?" disciplina2.cod_depto=" .$_SESSION['user']. " and":'';
			$q_tipo='';
			$q_status='';
			$q_atend=($_POST['atend']=='n') ? " matricula2.atendida='não' and" : "";
			$where='';
			if (!empty($_POST['aluno'])) $q_aluno=" lower(aluno.nome) like lower('%".$_POST['aluno']."%') and";
			if (!empty($_POST['curso_aluno'])) $q_curso_aluno=" lower(aluno.curso) like lower('%".$_POST['curso_aluno']."%') and";
			if (!empty($_POST['disc'])) {
				$q_disc=' (';
				$teste=explode(";",$_POST['disc']);
				foreach ($teste as $val){
					$q_disc.="lower(disciplina2.nome) like lower('%" .trim($val). "%') or lower(turmas.turma) like lower('%" .trim($val). "%') or "; //implementar disciplina2.cod_disc + turmas.turma
				}
				$q_disc=substr($q_disc,0,strlen($q_disc)-4);
				$q_disc.=") and";
			}
			if (!empty($_POST['depto_disc']) and $_SESSION['modulo']==1) $q_depto=" (disciplina2.cod_depto='".$_POST['depto_disc']."' or lower(depto.nome) like lower('%".$_POST['depto_disc']."%')) and";
			if (!empty($_POST['curso_disc'])) $q_curso_disc=" turmas.cod_curso=".$_POST['curso_disc']." and";
			if (!empty($_POST['tipo'])) $q_tipo=" modalidade.cod_mod = '" .$_POST['tipo']. "' and";
			if (!empty($_POST['status'])) $q_status=" matricula2.status = '" .$_POST['status']. "' and";
			$q_calend=" calendario.cod_calend = " . $_POST['calend'] . " and";
			$_SESSION["whereLote"]= ' where' . substr($q_aluno . $q_curso_aluno . $q_disc . $q_curso_disc . $q_depto . $q_tipo . $q_status . $q_calend . " matricula2.atendida='não' and",0,strlen($where)-4);
			$where = ' where' .$q_aluno . $q_curso_aluno . $q_disc . $q_curso_disc . $q_depto . $q_tipo . $q_status . $q_atend . $q_calend;
			$_SESSION['tipo']=(!empty($_POST['tipo'])) ? $_POST['tipo'] : "tdsMatr";
			$where=($where!=' where')?substr($where,0,strlen($where)-4):'';
			$_SESSION['where']=$where;
			$camposOcultosArray=(strpos($_POST['camposExtras'],'Parecer')!==false)?	array('matricula2.cod_matr' => 'codMatr') 	:	array('matricula2.cod_matr' => 'codMatr', 'matricula2.parecer' => 'Parecer');
			$camposOcultos='';
			foreach($camposOcultosArray as $key => $val){
				$camposOcultos.=$key . ' as ' . $val . ', ';
			}
			$camposFixosIni="aluno.nome as Aluno, concat(disciplina2.cod_disc, turmas.turma,' - ',disciplina2.nome) as Disciplina, modalidade.tipo as Tipo, ";
			$camposFixosFim="status.tipo as Status, matricula2.atendida as Atendida";
			$inner=" from matricula2 inner join aluno on matricula2.cod_aluno = aluno.cod_aluno inner join turmas on turmas.cod_turma=matricula2.cod_turma inner join disciplina2 on turmas.cod_disc=disciplina2.cod_disc inner join curso on turmas.cod_curso=curso.cod_curso inner join calendario on (turmas.semestre = calendario.semestre and turmas.ano = calendario.ano) inner join depto on disciplina2.cod_depto = depto.cod_depto inner join modalidade on matricula2.tipo=modalidade.cod_mod inner join status on matricula2.status=status.cod_status";
			$order=($_SESSION['user']=='grad')?" order by aluno.nome, disciplina2.nome, turmas.turma, aluno.cp desc, aluno.cr desc;":" order by disciplina2.nome, turmas.turma, aluno.cp desc, aluno.cr desc, aluno.nome;";
			$querycons="select " . $camposOcultos . $camposFixosIni . $_POST['camposExtras'] . $camposFixosFim . $inner . $where . $order;
			$_SESSION['query']="select " . $camposFixosIni . $_POST['camposExtras'] . $camposFixosFim . $inner . $where . $order;
			//echo json_encode($querycons);
			//break;
			$qryLista = mysqli_query($con, $querycons);
			$fields=mysqli_fetch_fields($qryLista);
			foreach ($fields as $val)
			{
				$header[]=$val->name;
			}
			$vetor[]=$header;
			while($resultado = mysqli_fetch_assoc($qryLista)){
				$vetor[] = $resultado; 
			}
			array_push($vetor,count($camposOcultosArray));
			echo json_encode($vetor);
			break;
			
		case 10: //alterar em lote. Usar a variável where para pegar os registros e a linha de comando  update matricula inner join aluno on matricula.cod_aluno=aluno.cod_aluno set matricula.status=STATUS where $where;
		// OK
			$vetor=array();
			$query="update matricula2 inner join aluno on matricula2.cod_aluno = aluno.cod_aluno inner join turmas on matricula2.cod_turma=turmas.cod_turma inner join disciplina2 on turmas.cod_disc=disciplina2.cod_disc inner join curso on turmas.cod_curso=curso.cod_curso inner join calendario on (turmas.semestre = calendario.semestre and turmas.ano = calendario.ano) inner join depto on disciplina2.cod_depto = depto.cod_depto inner join modalidade on matricula2.tipo=modalidade.cod_mod inner join status on matricula2.status=status.cod_status set matricula2.status='" .$_POST['status']. "'" .$_SESSION["whereLote"]. ";";
			$qryLista = mysqli_query($con, $query);  
			//echo json_encode($_SESSION["where"]);
			echo json_encode("ok");
			break;
			
		case 11: //preenche funcionalidades OK
			$vetor=array();
			$query="select  data as 'Data', titulo as 'Funcionalidade', descr as 'Descrição' from funcionalidades order by cod_func desc;";
			$qryLista = mysqli_query($con, $query);
			$fields=mysqli_fetch_fields($qryLista);
			foreach ($fields as $val)
			{
				$header[]=$val->name;
			}
			$vetor[]=$header;
			while($resultado = mysqli_fetch_assoc($qryLista)){
				$vetor[] = $resultado; 
			}
			echo json_encode($vetor);
			//echo json_encode('Sucesso');
			break;	
			
		case 12: //preenche resumo
		//ok
			$vetor=array();
			$result=array();
			if ($_SESSION['user']=='grad') { 
				$qry="select * from matricula2 inner join turmas on matricula2.cod_turma=turmas.cod_turma inner join calendario on (turmas.semestre = calendario.semestre and turmas.ano = calendario.ano) where calendario.cod_calend = ". $_POST['calend'] . ";";
			} else {
				$qry="select * from matricula2 inner join turmas on matricula2.cod_turma=turmas.cod_turma inner join disciplina2 on turmas.cod_disc=disciplina2.cod_disc inner join calendario on (turmas.semestre = calendario.semestre and turmas.ano = calendario.ano) where calendario.cod_calend = ". $_POST['calend'] . "and disciplina2.cod_depto=" .$_SESSION['user']. ";";
			}
			
			$qryLista=mysqli_query($con,$qry);
			while($resultado = mysqli_fetch_assoc($qryLista)){
				$vetor[] = $resultado; 
			}
			array_push($result,count($vetor));
			
			$vetor=array();
			if ($_SESSION['user']=='grad') { 
				$qry="select * from matricula2 inner join turmas on matricula2.cod_turma=turmas.cod_turma inner join calendario on (turmas.semestre = calendario.semestre and turmas.ano = calendario.ano) where calendario.cod_calend = ". $_POST['calend'] . " and not matricula2.status='Em análise';";
			} else {
				$qry="select * from matricula2 inner join turmas on matricula2.cod_turma=turmas.cod_turma inner join disciplina2 on matricula2.cod_disc=disciplina2.cod_disc where inner join calendario on (turmas.semestre = calendario.semestre and turmas.ano = calendario.ano) calendario.cod_calend = ". $_POST['calend'] . " and not matricula2.status='Em análise' and disciplina2.cod_depto=" .$_SESSION['user']. ";";
			}
			$qryLista=mysqli_query($con,$qry);
			while($resultado = mysqli_fetch_assoc($qryLista)){
				$vetor[] = $resultado; 
			}
			array_push($result,count($vetor));
			
			$vetor=array();
			if ($_SESSION['user']=='grad') { 
				$qry="select * from matricula2 inner join turmas on matricula2.cod_turma=turmas.cod_turma inner join calendario on (turmas.semestre = calendario.semestre and turmas.ano = calendario.ano) where calendario.cod_calend = ". $_POST['calend'] . " and matricula2.status='Em análise';";
			} else {
				$qry="select * from matricula2 inner join turmas on matricula2.cod_turma=turmas.cod_turma inner join disciplina2 on matricula2.cod_disc=disciplina2.cod_disc inner join calendario on (turmas.semestre = calendario.semestre and turmas.ano = calendario.ano) where calendario.cod_calend = ". $_POST['calend'] . " and matricula2.status='Em análise' and disciplina2.cod_depto=" .$_SESSION['user']. ";";
			}
			$qryLista=mysqli_query($con,$qry);
			while($resultado = mysqli_fetch_assoc($qryLista)){
				$vetor[] = $resultado; 
			}
			array_push($result,count($vetor));
			
			$vetor=array();
			if ($_SESSION['user']=='grad') { 
				$qry="select * from matricula2 inner join turmas on matricula2.cod_turma=turmas.cod_turma inner join calendario on (turmas.semestre = calendario.semestre and turmas.ano = calendario.ano) where calendario.cod_calend = ". $_POST['calend'] . " and matricula2.atendida='sim';";
			} else {
				$qry="select * from matricula2 inner join turmas on matricula2.cod_turma=turmas.cod_turma inner join disciplina2 on matricula2.cod_disc=disciplina2.cod_disc inner join calendario on (turmas.semestre = calendario.semestre and turmas.ano = calendario.ano) where calendario.cod_calend = ". $_POST['calend'] . " and matricula2.atendida='sim' and disciplina2.cod_depto=" .$_SESSION['user']. ";";
			}
			$qryLista=mysqli_query($con,$qry);
			while($resultado = mysqli_fetch_assoc($qryLista)){
				$vetor[] = $resultado; 
			}
			array_push($result,count($vetor));
			$vetor=array();
			
			
			echo json_encode($result);
		//	echo json_encode('User: ' .$_SESSION['user']);
			break;
			
		case 13: //load semestre
			$vetor=array();
			$qry="select cod_calend as Cod, concat(semestre,'º semestre de ',ano) as Semestre from calendario order by ano desc, semestre desc;";
			$qryLista=mysqli_query($con,$qry);
			while($resultado = mysqli_fetch_assoc($qryLista)){
					$vetor[] = $resultado; 
				}
			echo json_encode($vetor);
			break;
			
		case 14: //exibe os períodos letivos
			$vetor=array();
			$qry="select cod_calend as ID, concat(semestre,'º semestre de ',ano) as 'Período', ini_sem as 'Início', fim_sem as 'Término' from calendario order by ini_sem desc;";
			$qryLista=mysqli_query($con,$qry);
			$fields=mysqli_fetch_fields($qryLista);
			foreach ($fields as $val)
			{
				$header[]=$val->name;
			}
			$vetor[]=$header;
			while($resultado = mysqli_fetch_assoc($qryLista)){
				$vetor[] = $resultado; 
			}
			echo json_encode($vetor);
			break;
			
		case 15: //edita calendario	
			$vetor=array();
			$qry="select cod_calend as ID, concat (semestre, 'º semestre de ', ano) as 'Período', DATE_FORMAT(ini_sem, '%d/%m/%Y') as 'Início Semestre', DATE_FORMAT(fim_sem, '%d/%m/%Y') as 'Término Semestre', DATE_FORMAT(ini_ae, '%d/%m/%Y') as 'Início Aluno Especial', DATE_FORMAT(fim_ae, '%d/%m/%Y') as 'Término Aluno Especial', DATE_FORMAT(ini_aj, '%d/%m/%Y') as 'Início Ajustes', DATE_FORMAT(fim_aj, '%d/%m/%Y') as 'Término Ajustes', DATE_FORMAT(ini_eq, '%d/%m/%Y') as 'Início Equivalentes', DATE_FORMAT(fim_eq, '%d/%m/%Y') as 'Término Equivalentes', DATE_FORMAT(ini_ae, '%H:%i') as hora1, DATE_FORMAT(fim_ae, '%H:%i') as hora2, DATE_FORMAT(ini_aj, '%H:%i') as hora3, DATE_FORMAT(fim_aj, '%H:%i') as hora4, DATE_FORMAT(ini_eq, '%H:%i') as hora5, DATE_FORMAT(fim_eq, '%H:%i') as hora6 from calendario where cod_calend = " . $_POST['id'] . ";";
			//$qry="select * from aluno;";
			$qryLista=mysqli_query($con,$qry);
			
			$fields=mysqli_fetch_fields($qryLista);
			foreach ($fields as $val)
			{
				$header[]=$val->name;
			}
			$vetor[]=$header;
			
			while($resultado = mysqli_fetch_assoc($qryLista)){
				$vetor[] = $resultado; 
			}
			echo json_encode($vetor);
			$_SESSION['id_calendario']=$_POST['id'];
			break;
			
		case 16: //retorna o período letivo mais atual
			$vetor=array();
			$qry="select * from calendario order by ano desc, semestre desc limit 1;";
			$qryLista=mysqli_query($con, $qry);
			while($resultado = mysqli_fetch_assoc($qryLista)){
				$vetor[] = $resultado; 
			}
			echo json_encode($vetor);
			break;
			
		case 17: //inclui calendario
			$qry="insert into calendario values (0," . $_POST['ano'] . "," . $_POST['semestre'] . ",'" . $_POST['ini_ae'] . "','" . $_POST['fim_ae'] . "','" . $_POST['ini_aj'] . "','" . $_POST['fim_aj'] . "','" . $_POST['ini_eq'] . "','" . $_POST['fim_eq'] . "','" . $_POST['ini_sem'] . "','" . $_POST['fim_sem'] . "');";
			$qryLista=mysqli_query($con,$qry);
			echo json_encode('ok');
			//query funcionando, precisa converter a data antes de salvar
			break;
			
		case 18: //atualiza calendario
			$qry="update calendario set ini_ae='" . $_POST['ini_ae'] . "', fim_ae='" . $_POST['fim_ae'] . "', ini_aj='" . $_POST['ini_aj'] . "', fim_aj='" . $_POST['fim_aj'] . "', ini_eq='" . $_POST['ini_eq'] . "', fim_eq='" . $_POST['fim_eq'] . "', ini_sem='" . $_POST['ini_sem'] . "', fim_sem='" . $_POST['fim_sem'] . "' where cod_calend=" . $_SESSION['id_calendario'] . ";";
			$qryLista=mysqli_query($con,$qry);
			echo json_encode('ok');
			break;
			
			
		case 20: //verifica campos extras OK
			$camposExtras='';
			foreach($_POST as $key => $value) {
				if ($key!='page') $camposExtras.=str_replace('_','.',$key) . " as '" .$value."', ";
			}
			echo json_encode($camposExtras);
			break;
			
		case 21: //exclui calendario
			$qry="delete from calendario where cod_calend = " . $_POST['cod_calend'] . " and ano = " . $_POST['ano'] . " and semestre = " . $_POST['semestre'] . ";";
			$qryLista=mysqli_query($con,$qry);
			echo json_encode('ok');
			break;
			
		case 22: //upload de arquivos de aluno especial
			echo('teste');
			break;
			$uploaddir = '/var/www/html/new/8621adebeb5bab5879f9b2df4c02e1b5/';
			
			$arquivo = basename($_FILES['fileRg']['name']);
			$arquivo = explode('.', $arquivo);
			$arquivo = array_pop($arquivo);
			$arquivo =  md5(time()) . "." . $arquivo;
			$uploadfile = $uploaddir . $arquivo;
			if (move_uploaded_file($_FILES['fileRg']['tmp_name'], $uploadfile)) {
				echo "Upload do RG efetuado com sucesso!";
			} else {
				echo "Erro RG\n";
				
			}
			/*
			$arquivo = basename($_FILES['fileCpf']['name']);
			$arquivo = explode('.', $arquivo);
			$arquivo = array_pop($arquivo);
			$arquivo =  md5(time()) . "." . $arquivo;
			$uploadfile = $uploaddir . $arquivo;
			if (move_uploaded_file($_FILES['fileCpf']['tmp_name'], $uploadfile)) {
				echo "Upload do Cpf efetuado com sucesso!";
			} else {
				echo "Erro RG\n";
				exit;
			}
			
			$arquivo = basename($_FILES['fileComp']['name']);
			$arquivo = explode('.', $arquivo);
			$arquivo = array_pop($arquivo);
			$arquivo =  md5(time()) . "." . $arquivo;
			$uploadfile = $uploaddir . $arquivo;
			if (move_uploaded_file($_FILES['fileComp']['tmp_name'], $uploadfile)) {
				echo "Upload do Comprovante efetuado com sucesso!";
			} else {
				echo "Erro RG\n";
				exit;
			}
			*/
			break;
			
		case 23: //chamada pelo load select pra carregar os dados de uma tabela em um <select>
			$vetor=array();
			switch($_POST["objeto"]) {
				case 'modalidade':
					$table = 'modalidade';
					$value = 'cod_mod';
					$text = 'tipo';
					$order = 'tipo';
					break;
				
				case 'status':
					$table = 'status';
					$value = 'cod_status';
					$text = 'tipo';
					$order = 'tipo';
					break;
					
				case 'calendario':
					$table = 'calendario';
					$value = 'cod_calend';
					$text = "concat(semestre,'º semestre de ',ano) as semestre";
					$order = 'ano desc, semestre desc';
					break;
			}
			if ($table=='') {
				$vetor[] = ['cod' => 0, 'text' => $_POST["objeto"] . ' not found.'];
				echo json_encode($vetor);
				break;
			}
				
			$qry="select " .$value. ", " .$text. " from " .$table. " order by " .$order. ";";
			$qryLista=mysqli_query($con,$qry);
			while($resultado = mysqli_fetch_assoc($qryLista)){
					$vetor[] = $resultado; 
				}
			echo json_encode($vetor);
			break;
			
		case 100: //teste para atualizar bd a partir de arquivo do excel
			$qry="update aluno_teste set ra='', telefone='', email='', cr='', cp='', mp='', ativo='n' where universidade='FCAV';";
			$qryLista=mysqli_query($con,$qry);
			$qry=$_POST['qry'];
			//$qryLista=mysqli_query($con,$qry);
			echo json_encode($qry);
			break;
			
		case 2100: //teste
			$vetor=array();
			
			break;
	}


?>