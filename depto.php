<html>
<head>
<script src="../jquery-3.3.1.js" type="text/javascript"></script>
	<script src="../jquery.mask.js" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="../estilo.css">
</head>
<body>



	<div class="width100" id="divDeptoPesquisa">
		<table style='border-collapse: collapse;' frame="box" width="100%">
			<tr height='20'>
				<td rowspan='1'colspan='6' align ='left' valign ='bottom' style='font-weight: bold;font-style: italic;text-decoration: underline;color: rgb(0,0,0);'>Parâmetros da pesquisa:</td>
			</tr>
			<tr height='20'>
				<td  valign ='bottom' style='color: rgb(0,0,0);' colspan="6"></td>
			</tr>
			<tr height='20'>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'>Aluno</td>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'>Curso do aluno</td>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'>Disciplina</td>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'>Curso da disciplina</td>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'>Tipo de Matrícula</td>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'>Status</td>
			</tr>
			<tr height='20'>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'><input type='text' id='aluno' style="width: 100%;"></td>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'><input type='text' id='curso_aluno' style="width: 100%;"></td>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'><input type='text' id='disc' style="width: 100%;"></td>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'>
					<select id='curso_disc' style="width: 100%;" class="borda">
						<option value=""></option>
						<option value="72">Administração</option>
						<option value="45">Ciências Biológicas</option>
						<option value="18">Engenharia Agronômica</option>
						<option value="26">Medicina Veterinária</option>
						<option value="37">Zootecnia</option>
					</select>
				</td>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'>
					<select id='tipo' style="width: 100%;" class="borda">
						<option value=""></option>
						<option value="AE">Aluno Especial</option>
						<option value="AJ">Ajuste</option>
						<option value="EQ">Equivalentes</option>
					</select>
				</td>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'>
					<select id='status' style="width: 100%;" class="borda">
						<option value=""></option>
						<option value="Deferido">Deferido</option>
						<option value="Em análise">Em análise</option>
						<option value="Indeferido">Indeferido</option>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="6"><input type="checkbox" id="chkAtend" value="1"> Exibir solicitações já atendidas pela Seção de Graduação</td>
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
		</table>
	</div>
	<div class="linhaestreito">
		<input type="button" id="btBuscar" value="Pesquisar">
		<input type="button" id="btTeste" value="Teste" class='nvis'>
	</div>
	<div id="divBt" class="nvis">
		<select id="selLote">
			<option value="Deferido">Deferido</option>
			<option value="Em análise" selected>Em análise</option>
			<option value="Indeferido">Indeferido</option>
		</select>
		&nbsp;
		<input type="button" id="btChgStatus" value="Alterar todos os status" class="btPequeno">
	</div>
	<div id="divNotFound" class="nvis">
		<h3>Nenhuma solicitação de matrícula encontrada.</h3>
	</div>
	
	<div id="divXls" class="nvis">
		<form id="frmXls" method="post">
			<input type="text" id="txtFile" name="txtFile">
			<input type="text" id="txtQuery" name="txtQuery">
		</form>
	</div>
	
	<div id="divShowDados" class="nvis width100">
			<table id="tblDados" width= "100%" style="border-collapse: collapse; font-size: 14;" cellpadding="2">
				<tr height="20">
					<td id="tdAumentar">
						<table width="100%" border="0">
							<tr>
								<td align="left">Matrículas:</td>
								<td id="nReg" align="right"></td>
							</tr>
						</table> 
					</td>
				</tr>
				<tr id="trDados"><td colspan="6"></td></tr><tr><td colspan="6"></td></tr><tr><td colspan="6"></td></tr>			
			</table>
	</div>
	<p>
	<script>

	$("#btTeste").click(function() {
		console.log($('#chkAtend').prop('checked'));
	});
	
	
	
	//Clique no botão Alterar em lote
	$("#btChgStatus").click(function(){
		$.ajax({
			type:'post',
			data:{page: 10, status: $("#selLote").val()},
			dataType: 'json',
			url: 'check.php',
			success: function(dados){
				//console.log(dados);
				$("select.unico").not("[disabled]").val($("#selLote").val());
				$("select.unico").not("[disabled]").parent().find('div').removeClass('nvis').addClass('vis');
			},
		});
	});
	
	
	//Clique no botão buscar
	$("#btBuscar").click(function() {
		$atend='n';
		if ($('#chkAtend').prop('checked')) {
			$atend='s';
		}
		$.ajax({
			type:'post',
			data: {aluno: $('#aluno').val(), curso_aluno: $('#curso_aluno').val(), disc: $('#disc').val(), depto: 2089, curso_disc: $('#curso_disc').val(), tipo: $('#tipo').val(), status: $('#status').val(), atend: $atend, page: 9},
			dataType: 'json',
			url: 'check.php',
			success: function(dados){
				if (dados.length>1){
					$("tr.remover").each(function(i){
						$(this).remove();
					});
					$("#nReg").empty();
					$("#nReg").append(dados.length-1 + " solicitações de matrícula encontradas. <input type='button' id='btTsv' value='Exportar' class='btPequeno'>");
					$currentTr=$("#trDados");
					$inserir="<tr class='remover'>";
					$(dados[0]).each(function(i,header){
						$('#tdAumentar').attr('colspan',dados[0].length-1);
						if (i>0){
							$largura="";
							if (header=="RA") {$largura=" width='5%'";}
							if (header=="Aluno") {$largura=" width='15%'";}
							if (header=="Justificativa") {$largura=" width='35%'";}
							$inserir+="<td class='borda' style='font-weight: bold;' valing='top'" + $largura + ">" + header + "</td>";
						}
					});
					$inserir+="</tr>";
					$($inserir).insertAfter($currentTr)
					$currentTr=$currentTr.next();
					$(dados).each(function(i){
						if (i>=1){
							dados[i]['RA']=(dados[i]['RA']==null)?'':dados[i]['RA'];
							dados[i]['Coef. Matr.']=(dados[i]['Coef. Matr.']==null)?'':dados[i]['Coef. Matr.'];
							dados[i]['Coef. Rend.']=(dados[i]['Coef. Rend.']==null)?'':dados[i]['Coef. Rend.'];
							var $inserir='<tr class="remover"><td class="borda">'+dados[i].RA+'</td><td class="borda">'+dados[i].Aluno+'</td><td class="borda">'+dados[i]['Coef. Matr.']+'</td><td class="borda">'+dados[i]['Coef. Rend.']+'</td><td class="borda">'+dados[i].Curso+'</td><td class="borda">'+dados[i].Disciplina+'</td><td class="borda">'+dados[i].Tipo+'</td><td class="borda">'+dados[i].Justificativa+'</td><td class="borda">';
							$disable='';
							if (dados[i].Atendida=='sim') {$disable='disabled';}
							if (dados[i].Status == "Em análise") {
								$inserir += '<label class="nvis">'+ dados[i].codMatr +'</label><select class="unico"'+$disable+'><option value="Deferido">Deferido</option><option value="Em análise" selected>Em análise</option><option value="Indeferido">Indeferido</option></select>';
							} else if (dados[i].Status == "Deferido") {
								$inserir += '<label class="nvis">'+ dados[i].codMatr +'</label><select class="unico"'+$disable+'><option value="Deferido" selected>Deferido</option><option value="Em análise">Em análise</option><option value="Indeferido">Indeferido</option></select>';
							} else if (dados[i].Status == "Indeferido") {
								$inserir += '<label class="nvis">'+ dados[i].codMatr +'</label><select class="unico"'+$disable+'><option value="Deferido">Deferido</option><option value="Em análise">Em análise</option><option value="Indeferido" selected>Indeferido</option></select>';
							} else { //status com erro
								$inserir += dados[i].Status+'</td>';
							}
							$inserir += "<br><div class='nvis alter'>Salvo!</div></td><td class='borda'>"+dados[i].Atendida+"</td></tr>";

							$($inserir).insertAfter($currentTr);
							
							$currentTr=$currentTr.next();
						}
					});
					$("#divShowDados").removeClass("nvis").addClass("vis");
					$("#divNotFound").removeClass("vis").addClass("nvis");
					$("#divBt").removeClass("nvis").addClass("vis linhalargo");
				} else { //nao encontrou
					$("#divNotFound").removeClass("nvis").addClass("vis");
					$("#divShowDados").removeClass("vis").addClass("nvis");
					$("#divBt").removeClass("vis linhalargo").addClass("nvis");
				}
			},
			complete: function(){
				$('select.unico').change(function(){
					var $cel = $(this).parent();
					var $cod_matr=$cel.find("label").html();
					console.log ($cod_matr);
					var $status=$(this).val();
					$.ajax({
						type:'post',
						data: {cod_matr: $cod_matr, status: $status, page: 8},
						dataType: 'json',
						url: 'check.php',
						success: function(dados){
							$cel.find("div").removeClass("nvis").addClass("vis");
						},
					});
				});

				$("#btTsv").click(function(){
					$.ajax({
						type:'post',
						data: {page: 7},
						dataType: 'json',
						url: 'check.php',
						success: function(dados){
							//console.log(dados);
							$("#txtFile").val(dados[0]);
							$("#txtQuery").val(dados[1]);
							$("#frmXls").attr('action','GeraXls.php');
							$("#frmXls").submit();
						},
					});
				});
			}
		});
	});
	
//alteração de status de matrícula
	
	
	</script>
	
	
	</body>
	</html>