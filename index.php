<?php 
	session_start(); 
	date_default_timezone_set('America/Sao_Paulo');
	$calend=include('start_vars.php');
	$_SESSION['cod_calend']=$calend['codCalend'];
?>
<html lang="pt-br">
	<head>
		<title>Matrícula de Aluno Especial - FCAV - 2018</title>
		<script src="jquery-3.3.1.js" type="text/javascript"></script>
		<script src="jquery.mask.js" type="text/javascript"></script>
		<link rel="stylesheet" type="text/css" href="estilo.css">
		<link rel="shortcut icon" href="">
		<meta charset="utf-8">
	</head>
	<body>
		<?php
		$hoje = strtotime(date('Y/m/d H:i',time())); 
		$_SESSION['tipo']=(!empty($_POST['tipo'])) ? $_POST['tipo'] : "tdsMatr";
		$ae=(($hoje>=strtotime($calend['iniAe'])) and ($hoje<=strtotime($calend['fimAe']))) ? 1:0;
		$aj=(($hoje>=strtotime($calend['iniAj'])) and ($hoje<=strtotime($calend['fimAj']))) ? 1:0;
		$eq=(($hoje>=strtotime($calend['iniEq'])) and ($hoje<=strtotime($calend['fimEq']))) ? 1:0;
		if (($ae+$aj+$eq)>0){
		?>
		<div id='divMain' class='vis'>
			<div id='divParam' class='nvis'>
				<input id='txtParamAe' type='text' value='<?php echo($ae) ?>'>
				<input id='txtParamAj' type='text' value='<?php echo($aj) ?>'>
				<input id='txtParamEq' type='text' value='<?php echo($eq) ?>'>
			</div>
	
			<div id="divInicio" class="vis">
				<span style="background-color: #ff9090; display: block;"><h3>Atenção: Houve um problema e as primeiras solicitações feitas até a manhã do dia 22/07/19 foram perdidas. Consulte sua matrícula <b><a href="http://www.fcav.unesp.br/#!/graduacao/espaco-do-aluno/matriculaconsultaaluno/" target="_blank">AQUI</a></b> e, caso não esteja salva, faça a solicitação novamente.</h3></span>
				<h4>Entre com o CPF:</h4>
				<table style="border-collapse: collapse;">
					<tr height="20">
						<td id="td1"><input type="text" id="iniCpf" maxlength='14' name="iniCpf" class="borda verVazio"></td><td><label class="nvis"> Campo obrigatório</label></td>
					</tr>
				</table>
				<p>
			</div>
	
			<div id="divDados" class="nvis">
				<h4>Dados pessoais:</h4>
				<span id="errAluno" align="left" class="nvis" style="color: #bf1e2c;"><h4>Aluno não cadastrado. Preencha todos os campos</h4></span>
				<table style="border-collapse: collapse;">
					<tr height="20">
						<td valign ="bottom" style="font-weight: bold;color: rgb(0,0,0);"align="right">Nome:  </td>
						<td><input type="text" id="dtNome" name="dtNome" size="40" maxlength="100" class="borda verVazio"></td>
						<td><label class="nvis"> Campo obrigatório</label></td>
					</tr>
					<tr height="20">
						<td  valign ="bottom" style="font-weight: bold;color: rgb(0,0,0);"align="right">RG:  </td>
						<td><input type="text" id="dtRg" name="dtRg" size="40"maxlength="20" class="borda verVazio"></td>
						<td><label class="nvis"> Campo obrigatório</label></td>
					</tr>
					<tr height="20">
						<td  valign ="bottom" style="font-weight: bold;color: rgb(0,0,0);"align="right">CPF:  </td>
						<td><input type="text" id="dtCpf" name="dtCpf" size="40"maxlength="14" disabled class="borda verVazio"></td>
						<td><label class="nvis"> Campo obrigatório</label></td>
					</tr>
					<tr height="20">
						<td  valign ="bottom" style="font-weight: bold;color: rgb(0,0,0);"align="right">Universidade:  </td>
						<td><input type="text" id="dtUniv" name="dtUniv" size="40" maxlength="50" class="borda verVazio"></td>
						<td><label class="nvis"> Campo obrigatório</label></td>
					</tr>
					<tr height="20">
						<td  valign ="bottom" style="font-weight: bold;color: rgb(0,0,0);"align="right">Curso:  </td>
						<td><input type="text" id="dtCurso" name="dtCurso" size="40"maxlength="30" class="borda verVazio"></td>
						<td><label class="nvis"> Campo obrigatório</label></td>
					</tr>
					<tr height="20">
						<td  valign ="bottom" style="font-weight: bold;color: rgb(0,0,0);"align="right">Telefone:  </td>
						<td><input id="dtTel" type="text" id="dtTel" name="dtTel" size="40"maxlength="15" class="borda verVazio"></td>
						<td><label class="nvis"> Campo obrigatório</label></td>
					</tr>
					<tr height="20">
						<td  valign ="bottom" style="font-weight: bold;color: rgb(0,0,0);"align="right">Email:  </td>
						<td><input id="dtEmail" type="text" id="dtEmail" name="dtEmail" size="40"maxlength="30" class="borda verVazio"></td>
						<td><label class="nvis"> Campo obrigatório</label></td>
					</tr>
				</table>
			</div>
		
		<!-- copiar -->
			
		<!-- copiar até aqui -->
		
		
			<div id="divDisc" class="nvis">
				<h4>Tipo de Matrícula</h4>
					<div id="divAj"><input type="radio" name="rdTipo" id="rdTipo" value="AJ">Ajuste de matrícula</div>
					<div id="divAe"><input type="radio" name="rdTipo" id="rdTipo" value="AE">Aluno Especial</div>
					<div id="divEq"><input type="radio" name="rdTipo" id="rdTipo" value="EQ">Matrícula em disciplinas equivalentes de outros cursos</div>

				<div id='divFiles' class='nvis'>
					<h4>Anexar Documentos (<u>Formato PDF</u>) - Somente para alunos de fora da FCAV e ex-alunos da FCAV.</h4>
					<input type='checkbox' id='checkFiles' value='sent'> <label for='checkFiles'> Sou aluno da FCAV ou já enviei os documentos.</label><p>
					<input type='button' id='btlUpRg' class='btPequeno' value='Anexar RG...' style='width: 200px;' >
					<input id="upRg" name="upRg" type="file">&nbsp;<span id="txtFile"></span><p>
					<input type='button' id='btlUpCpf' class='btPequeno' value='Anexar CPF...' style='width: 200px;' >
					<input id="upCpf" name="upCpf" type="file">&nbsp;<span id="txtFile"></span><p>
					<input type='button' id='btlUpComprovante' class='btPequeno' value='Anexar Atestado de Matrícula, Histórico ou Diploma...' style='width: 200px; white-space: pre-line;' >
					<input id="upComprovante" name="upComprovante" type="file">&nbsp;<span id="txtFile"></span><br>
				</div>
				
				<h4>Selecione o curso que deseja fazer a disciplina:</h4>
				<select id="selCurso" class="borda">
					<option value="0" selected>Selecione um curso...</option>
				</select>
				<p>
				
				<h4>Disciplina&emsp;<label id="lblRepetido" class="nvis" style="color: #bf1e2c;"><b>*&nbsp;Solicitação de matrícula já efetuada. Selecione outra disciplina e/ou turma.&nbsp;</b></label></h4>
							
				
							<select id="selDisc" style="max-width: 95%;" class="borda">
								<option value="0">Selecine uma disciplina...</option>
							</select>
						
				
				<p>	
				<div id="divTurma" class="vis">
				<h4>Turma Prática:</h4>
					<select id="selTurma" style="max-width: 95%" class="borda">
						<option value="0">Disciplina sem turma prática</option>
					</select>
				</div>
				<p>
				<b><u>ATENÇÃO:</u></b> Consulte o horário das disciplinas <a href="http://www.fcav.unesp.br/#!/graduacao/espaco-do-aluno/horario-das-disciplinas/" target="_blank">AQUI</a>. Matrículas com choque de horário serão indeferidas.
				<h4>Justificativa:</h4>
				<textarea name="justificativa" rows="5" maxlength="220" style="resize:none; width: 600px;" id="txtJustif" class="justif"></textarea><label class="nvis"> Campo Obrigatório</label>
			</div>
		
			<div id="divFinal" class="nvis">
				
			</div>
		
		
			<div id="divControle" class="vis">
				<table style="border-collapse: collapse;" width="100%">
					<tr>
						<td width="50%" align="center"><input type="button" id="voltar" name="voltar" value="Voltar" disabled></td>
						<td width="50%" align="center"><input type="button" id="avancar" name="avancar" value="Avançar" disabled width="80%"></td>
					</tr>
				</table>
			</div>
			<p>
			<div id="divMatricula" class="nvis">
				<table style="border-collapse: collapse;" width="100%">
					<tr id='trWait' class='nvis'>
						<td width="100%" align="center"><span style="font-weight: bold;">Aguarde. Analisando a solicitação.</span></td>
					</tr>
					<tr>
						<td width="100%" align="center"><input type="button" id="salvar" name="salvar" value="Solicitar Matrícula" disabled></td>
					</tr>
				</table>
			</div>
			<div id="divRestart" class="nvis">
				<table style="border-collapse: collapse;" cellpadding='10'>
					<tr>
						<td align="center"><input type="button" id="outra" name="outra" value="Solicitar outra disciplina"></td>
						<td align="center"><input type="button" id="btInicio" name="btInicio" value="Voltar ao início"></td>
					</tr>
				</table>
			</div>
		</div>
		
		<div id='divNotAe' class='nvis'>
			<h3>Fora do Período de Matrícula</h3>
			<b>Matrícula de Aluno Especial:</b> <i>de <?php echo(date("<\u>d/m/Y</\u> - H:i\h", strtotime($calend['iniAe'])))?> à <?php echo(date("<\u>d/m/Y</\u> - H:i\h;", strtotime($calend['fimAe'])))?></i><p>
			<input type="button" id="btInicio2" value="Voltar ao início">
		</div>

	<script>
//inicia os scripts depois de carregar a pagina
	
		$(document).ready(function () { 
			var $page=1;
			var $vazio=false;
			
			
			
			
//mascara do cpf
			$("#iniCpf").mask('000.000.000-00', {reverse: true});
			
			
			$('#btInicio2').click(function(){
				$page=1;
				$('#btInicio').trigger('click');
			});
			
			$('#checkFiles').change(function(){
				//alert('teste \n quebra de linha');
				if ($(this).prop('checked')==true){
					$('#divFiles input[type=button]').attr('disabled', 'disabled');
					$('#divFiles input[type=file]').val('');
					$('#divFiles span').html('');
				} else {
					$('#divFiles input[type=button]').removeAttr('disabled');
				}
			});
			
//upload
			$('#divDisc input[type=button]').click(function(){
				$(this).next().trigger('click');
			});
			
			
			$('#divFiles input[type=file]').change(function(){
				$nomeArquivo = $(this).val();
				$nomeArquivo = $nomeArquivo.split('\\').pop();
				$extensao = $nomeArquivo.split('.').pop().toLowerCase();
				if ($extensao == 'pdf'){
					$(this).next().html($nomeArquivo);
				} else {
					alert('O formato do arquivo deve ser PDF.');
					$(this).val('');
				}
			});

			
			
//change Radio
			$('#divDisc input[type=radio]').on('change', function(){
				$tipoMatricula = $('#rdTipo:checked').val();
				if ($tipoMatricula == 'AE') {
					$('#divFiles').removeClass('nvis').addClass('vis');
					$('#divFiles input[type=button]').removeAttr('disabled');
				} else {
					$('#checkFiles').prop('checked',false);
					$('#divFiles').removeClass('vis').addClass('nvis');
					$('#divFiles input[type=button]').attr('disabled', 'disabled');
					$('#divFiles input[type=file]').val('');
					$('#divFiles span').html('');
				}
				$.ajax({
					type: 'post',
					data: {curso: $('#dtCurso').val(), tipo: $('#rdTipo:checked').val(), universidade: $('#dtUniv').val()},
					dataType: 'json',
					url: 'fillcurso.php',
					success: function(dados){
						$('#selCurso')[0].options.length = 0;
							$(dados).each(function(i){
								$('#selCurso').append($('<option>', {value: dados[i].cod_curso, text: dados[i].nome}));
							});
					},
					complete: function(){
						$.ajax({
							type: 'post',
							data: {curso: $('#selCurso').val(), page: 2},
							dataType: 'json',
							url: 'fillSel.php',
							success: function(dados){
								$('#selDisc')[0].options.length = 0;
								$(dados).each(function(i){
									$('#selDisc').append($('<option>', {tipo: dados[i].tipo, cod_disc: dados[i].cod_disc, turma: dados[i].turma, value: dados[i].cod_turma, text: dados[i].cod_disc + dados[i].turma + " - " + dados[i].nome}));
								});
							}
						});
						
					}
					
				});
				if ($('#txtJustif').val()=="") {
					$("#salvar").attr("disabled", "disabled");
				} else {
					$("#salvar").removeAttr("disabled");
				};
			});
			
//clique no botão inicio
			$("#btInicio").click(function(){
				//$("#divControle").css("width",$("#divInicio").css("width"));
				$('#iniCpf').val('');
				$("#divDados input[type=text]").each(function(){
					$(this).val("");
				});
				$("input:radio").prop('checked', false);
				$("#voltar").attr("disabled","disabled");
				$("#avancar").attr("disabled","disabled");
				$('#selDisc')[0].options.length = 0;
				$('#selDisc').append($('<option>', {value: 0, text: 'Selecione uma disciplina...'}));
				$('#selCurso')[0].options.length = 0;
				$('#selCurso').append($('<option>', {value: 0, text: 'Selecione uma curso...'}));
				$('#selTurma')[0].options.length = 0;
				$('#selTurma').append($('<option>', {value: 0, text: 'Disciplina sem turma prática'}));
				$('#selDisc').removeClass('vazio').addClass('borda');
				$('#selTurma').removeClass('vazio').addClass('borda');
				$('#lblRepetido').removeClass('vis').addClass('nvis');
				$("#salvar").attr("disabled","disabled");
				$("#txtJustif").val("");
				
				$('#divFiles').removeClass('vis').addClass('nvis');
				$('#divFiles input[type=button]').attr('disabled', 'disabled');
				$('#divFiles input[type=file]').val('');
				$('#divFiles span').html('');
				$('#checkFiles').prop('checked',false);
				
				$('#divRestart').removeClass('vis').addClass('nvis');
				$('#divFinal').removeClass('vis').addClass('nvis');
				$('#divFinal').html('');
				
				$('#divNotAe').removeClass('vis').addClass('nvis');
				
				$("#divInicio").removeClass("nvis").addClass("vis");
				$("#divControle").removeClass("nvis").addClass("vis");
				//$('#divMain').removeClass('nvis').addClass('vis');
				
				$page=1;
			});
			
//clique no botão outra
			$("#outra").click(function(){
				$("input:radio").prop('checked', false);
				//$("input:radio").removeAttr("checked");
				$('#selDisc')[0].options.length = 0;
				$('#selDisc').append($('<option>', {value: 0, text: 'Selecione uma disciplina...'}));
				$('#selCurso')[0].options.length = 0;
				$('#selCurso').append($('<option>', {value: 0, text: 'Selecione uma curso...'}));
				$('#selTurma')[0].options.length = 0;
				$('#selTurma').append($('<option>', {value: 0, text: 'Disciplina sem turma prática'}));
				$('#selDisc').removeClass('vazio').addClass('borda');
				$('#selTurma').removeClass('vazio').addClass('borda');
				$('#lblRepetido').removeClass('vis').addClass('nvis');
				$("#salvar").attr("disabled","disabled");
				$("#txtJustif").val("");
				
				$('#divFiles').removeClass('vis').addClass('nvis');
				$('#divFiles input[type=button]').attr('disabled', 'disabled');
				$('#divFiles input[type=file]').val('');
				$('#divFiles span').html('');
				$('#checkFiles').prop('checked',true);
				
				$("#divDisc").removeClass("nvis").addClass("vis");
				$("#divMatricula").removeClass("nvis").addClass("vis");
				$('#divRestart').removeClass('vis').addClass('nvis');
				$('#divFinal').removeClass('vis').addClass('nvis');
				$('#divFinal').html('');
				
				
				$page=3;
			});
			
//clique no botão salvar
			$("#salvar").click(function(){
				if ($page==3){ // vai para a página que seleciona curso, disciplina e justificativa
					$cod_turma=($('#selTurma').val()!=0)?$('#selTurma').val():$('#selDisc').val();
					$cpf = $('#dtCpf').val();
					// anexar arquivos aqui
					
					var matricular=false;
					var form_data = new FormData();
					form_data.append('jaUpou', $('#checkFiles').prop('checked'));
					form_data.append('cpf', $cpf);
					//form_data.append('ativo', $ativo);
					$sair=false;
					$('#divFiles input[type=file]').each(function(){
						var file_data = $(this).prop("files")[0];   
						form_data.append('arquivos[]', file_data);
					});

					$.ajax({
						url: 'upload.php', 
						dataType: 'text',
						cache: false,
						contentType: false,
						processData: false,
						//async: false,
						data: form_data,                         
						type: 'post',
						beforeSend: function(){
							$('#trWait').removeClass('nvis').addClass('vis');
						},
						success: function(dtUpload){
							if (dtUpload.substr(0,10)=='Arquivo(s)') { //marcou o campo mas tá faltando
								alert('Há arquivos faltando ou corrompidos. Anexe novamente.');
								$('#checkFiles').prop('checked',false);
								$('#divFiles input[type=button]').removeAttr('disabled');
								return false;
							}
							if (dtUpload.split('@')[0] == 'erro') {
								alert('Erro no arquivo ' + dtUpload.split('@').pop());
								return false;
							} 
							if (dtUpload == 'falta arquivos') {
								console.log('aqui');
								alert('Faça o upload de todos os arquivos');
								return false;
							} 
							if (dtUpload == 'ok') { //sucesso, limpar os campos
								matricular=true;
								$('#divFiles input[type=file]').each(function(){
									$(this).val('');
								});
								$('#divFiles span').each(function(){
									$(this).html('');
								});
								
								///////////////////////////////////////////////////////// /*
								/*
								$.ajax({
									type:'post',
									data: {cod_disc: $cod_turma, tipo: $("#rdTipo:checked").val(), justif: $("#txtJustif").val(),  page: 3},
									dataType: 'json',
									url: 'check.php',
									success: function(dados){
										if (dados=='repetida'){
											$('#selDisc').removeClass('borda').addClass('vazio');
											$('#selTurma').removeClass('borda').addClass('vazio');
											$('#lblRepetido').removeClass('nvis').addClass('vis');
										} else if (dados=='uploadNotOk'){
											alert('Erro no upload de arquivos. Tente novamente.');
										} else {
											$("#divDisc").removeClass("vis").addClass("nvis");
											$("#divMatricula").removeClass("vis").addClass("nvis");
											$('#selDisc').removeClass('vazio').addClass('borda');
											$('#selTurma').removeClass('vazio').addClass('borda');
											$('#lblRepetido').removeClass('vis').addClass('nvis');
											$("#divRestart").removeClass("nvis").addClass("vis");
											if (dados.length=1){ //matricula feita com sucesso
												$html="<b>Matrícula solicitada como segue:</b><p><table style='border-collapse: collapse; font-size: 14;' cellpadding='3'>";
												$html+="<tr><td class='borda'>Nome: </td><td class='borda'>"+dados[0].Nome+"</td></tr>";
												$html+="<tr><td class='borda'>Disciplina: </td><td class='borda'>"+dados[0].Disciplina+"</td></tr>";
												$html+="<tr><td class='borda'>Curso: </td><td class='borda'>"+dados[0].Curso+"</td></tr>";
												$html+="<tr><td class='borda'>Tipo de Matrícula: </td><td class='borda'>"+dados[0].Tipo+"</td></tr>";
												$html+="<tr><td class='borda'>Justificativa: </td><td class='borda'>"+dados[0].Justificativa+"</td></tr>";
												$html+="<tr><td class='borda'>Data/Hora: </td><td class='borda'>"+dados[0].Data+"</td></tr>";
												$html+="</table><p>";
												$html+="<b>ATENÇÃO: </b>Acompanhe o andamento de sua solicitação <a href='http://www.fcav.unesp.br/#!/graduacao/espaco-do-aluno/matriculaconsultaaluno/' target='_blank'>AQUI</a>. A 	divulgação do resultado das solicitações serão informadas no referido link.";
											} else {//erro
												$html="<b>Erro na matrícula!</b><br>Entre em contato com a Seção de Graduação!";
											}
											$('#divFinal').html($html);
											$("#divFinal").removeClass("nvis").addClass("vis");
										}
									},
								});
								
								*/////////////////////////////////////////////
							}
						},	
						complete: function(){
							$('#trWait').removeClass('vis').addClass('nvis');
							if (matricular==false) return false;
							$.ajax({
									type:'post',
									data: {cod_disc: $cod_turma, cpf: $cpf, tipo: $("#rdTipo:checked").val(), justif: $("#txtJustif").val(),  page: 3},
									dataType: 'json',
									url: 'check.php',
									success: function(dados){
										if (dados=='repetida'){
											$('#selDisc').removeClass('borda').addClass('vazio');
											$('#selTurma').removeClass('borda').addClass('vazio');
											$('#lblRepetido').removeClass('nvis').addClass('vis');
										} else if (dados=='uploadNotOk'){
											alert('Erro no upload de arquivos. Tente novamente.');
										} else {
											$("#divDisc").removeClass("vis").addClass("nvis");
											$("#divMatricula").removeClass("vis").addClass("nvis");
											$('#selDisc').removeClass('vazio').addClass('borda');
											$('#selTurma').removeClass('vazio').addClass('borda');
											$('#lblRepetido').removeClass('vis').addClass('nvis');
											$("#divRestart").removeClass("nvis").addClass("vis");
											if (dados.length=1){ //matricula feita com sucesso
												$html="<b>Matrícula solicitada como segue:</b><p><table style='border-collapse: collapse; font-size: 14;' cellpadding='3'>";
												$html+="<tr><td class='borda'>Nome: </td><td class='borda'>"+dados[0].Nome+"</td></tr>";
												$html+="<tr><td class='borda'>Disciplina: </td><td class='borda'>"+dados[0].Disciplina+"</td></tr>";
												$html+="<tr><td class='borda'>Curso: </td><td class='borda'>"+dados[0].Curso+"</td></tr>";
												$html+="<tr><td class='borda'>Tipo de Matrícula: </td><td class='borda'>"+dados[0].Tipo+"</td></tr>";
												$html+="<tr><td class='borda'>Justificativa: </td><td class='borda'>"+dados[0].Justificativa+"</td></tr>";
												$html+="<tr><td class='borda'>Data/Hora: </td><td class='borda'>"+dados[0].Data+"</td></tr>";
												$html+="</table><p>";
												$html+="<b>ATENÇÃO: </b>Acompanhe o andamento de sua solicitação <a href='http://jade.fcav.unesp.br/consulta_aluno.php' target='_blank'>AQUI</a>. A 	divulgação do resultado das solicitações serão informadas no referido link.";
											} else {//erro
												$html="<b>Erro na matrícula!</b><br>Entre em contato com a Seção de Graduação!";
											}
											$('#divFinal').html($html);
											$("#divFinal").removeClass("nvis").addClass("vis");
										}
									},
								});
								///////////////////////
							
						}


					});
					
					
					
					
				}
			});
			
			
			
		
//clique no botão avançar
			$("#avancar").click(function(){
				if ($page==1){ //vai pra página de dados pessoais
					$("#errAluno").removeClass("vis").addClass("nvis");
					$("#divDados input[type=text]").each(function(i){
						$(this).removeAttr("disabled");
					});
					$("#voltar").removeAttr("disabled");
					$("#divDados input[type=text]").each(function(i){
						$(this).val("");
					});
					$("#divInicio").removeClass("vis").addClass("nvis");
					$("#divDados").removeClass("nvis").addClass("vis");
					
						$.ajax({
							type:'post',
							data: {cpf: $('#iniCpf').val(), page: 1},
							dataType: 'json',
							cache: false,
							url: 'check.php',
							success: function(dados){
								console.log(dados);
								if (dados!=null){
									$tipoCons="update";
									$("#dtNome").val(dados.nome);
									$("#dtNome").attr("disabled", "disabled");
									$("#dtRg").val(dados.rg);
									$("#dtRg").attr("disabled", "disabled");
									$("#dtCpf").val(dados.cpf);
									$("#dtCpf").attr("disabled", "disabled");
									$("#dtUniv").val(dados.universidade);
									$("#dtUniv").attr("disabled", "disabled");
									$("#dtCurso").val(dados.curso);
									$("#dtCurso").attr("disabled", "disabled");
									$("#dtTel").val(dados.telefone);
									$("#dtEmail").val(dados.email);
									if (!$("#dtTel").val() || !$("#dtEmail").val()){
										$("#avancar").attr("disabled","disabled");
									}
								}else {
									$tipoCons="insert";
									$("#errAluno").removeClass("nvis").addClass("vis");
									$("#avancar").attr("disabled","disabled");
									$("#dtCpf").val($("#iniCpf").val());
									$("#dtCpf").attr("disabled","disabled");
								}
							},
							complete: function(dados){

								$page=2;
							},
						});
				} else if ($page==2){ //vai pra página de curso e disciplina
					$('#selDisc').removeClass('vazio').addClass('borda');
					$('#lblRepetido').removeClass('vis').addClass('nvis');
					$.ajax({
						type: 'post',
						data: {cpf: $("#dtCpf").val(), page: 2, cons: $tipoCons, tel: $("#dtTel").val(), email: $("#dtEmail").val(), nome: $("#dtNome").val(), rg: $("#dtRg").val(), univ: $("#dtUniv").val(), curso: $("#dtCurso").val()},
						dataType: 'json',
						url: 'check.php',
						success: function(dados){
							console.log ('fora/dentro: ');
							console.log(dados);
							/////	VERIFICAR AQUI DEPOIS. verificar o que?
							
							if (dados=='fora'){ //libera apenas aluno especial
								if ($('#txtParamAe').val()==1){
									$('#divAe').removeClass('nvis').addClass('vis');
									$('#divAj').removeClass('vis').addClass('nvis');
									$('#divEq').removeClass('vis').addClass('nvis');
								} else{ // barrar upload de arquivos
									$('#divDados').removeClass('vis').addClass('nvis');
									$('#divControle').removeClass('vis').addClass('nvis');
									$('#divNotAe').removeClass('nvis').addClass('vis');
									
								}
							} else { //libera tudo
								if ($('#txtParamAe').val()==1){ 
									$('#divAe').removeClass('nvis').addClass('vis'); 
								} else { 
									$('#divAe').removeClass('vis').addClass('nvis');
								}
								if ($('#txtParamAj').val()==1){ 
									$('#divAj').removeClass('nvis').addClass('vis'); 
								} else { 
									$('#divAj').removeClass('vis').addClass('nvis');
								}
								if ($('#txtParamEq').val()==1){ 
									$('#divEq').removeClass('nvis').addClass('vis'); 
								} else { 
									$('#divEq').removeClass('vis').addClass('nvis');
								}
								$("#divDados").removeClass("vis").addClass("nvis");
								$("#divDisc").removeClass("nvis").addClass("vis");
								$("#divControle").removeClass("vis").addClass("nvis");
								$("#divMatricula").removeClass("nvis").addClass("vis");
							}
							$("#divDados").removeClass("vis").addClass("nvis");
								$("#divDisc").removeClass("nvis").addClass("vis");
								$("#divControle").removeClass("vis").addClass("nvis");
								$("#divMatricula").removeClass("nvis").addClass("vis");
						},
						complete: function(){
								$page=3;
							},
					});
					
				} 
			});
			
//clique no botão voltar
			$("#voltar").click(function(){				
				if ($page==2){
					if ($("#iniCpf").val()==""){
						$("#avancar").attr("disabled", "disabled");
					} else{
						$("#avancar").removeAttr("disabled");
					}
					$("#divDados").removeClass("vis").addClass("nvis");
					$("#divInicio").removeClass("nvis").addClass("vis");
					$(this).attr("disabled","disabled");
					$page=1;
				} else if ($page==3){
					$("#divDisc").removeClass("vis").addClass("nvis");
					$("#divDados").removeClass("nvis").addClass("vis");
					$("#avancar").removeAttr("disabled");
					$page=2;
				}
			});

//deixa o botão 'avançar' habilitado se o cpf estiver preenchido
			$(function(){
				if ($("#iniCpf").val()==""){
					$("#avancar").attr("disabled", "disabled");
				} else{
					$("#avancar").removeAttr("disabled");
				}
			});

			
			
//verifica o preenchimento do cpf ao sair do foco	
			$("#iniCpf").blur(function(){
				if ($(this).val()==""){
					$(this).parent().next().find("label").removeClass("nvis").addClass("vis");
					$(this).addClass("vazio");
					$("#avancar").attr("disabled", "disabled");
				} else{
					$(this).parent().next().find("label").removeClass("vis").addClass("nvis");
					$(this).removeClass("vazio");
					$("#avancar").removeAttr("disabled");
				}
			});

//verifica o preenchimento do cpf ao digitar
			$("#iniCpf").on("propertychange change keyup paste input", function(){
				if ($(this).val().length<14){
					$("#avancar").attr("disabled", "disabled");
				} else{
					$(this).parent().next().find("label").removeClass("vis").addClass("nvis");
					$(this).removeClass("vazio");
					$("#avancar").removeAttr("disabled");
				}
			});
			
//verifica o preenchimento dos campos do divDados ao sair do foco	
			$("#divDados input[type=text]").blur(function(){
				if ($(this).val()==""){
					$(this).parent().next().find("label").removeClass("nvis").addClass("vis");
					$(this).addClass("vazio");
					$("#avancar").attr("disabled", "disabled");
				} else{
					$(this).parent().next().find("label").removeClass("vis").addClass("nvis");
					$(this).removeClass("vazio");
					$("#avancar").removeAttr("disabled");
				}
				vazio=false;
				$("#divDados input[type=text]").each(function(i){
					if ($.trim($(this).val())==""){
						vazio=true;
						return;
					}
				});
				if (!vazio) {
					$("#avancar").removeAttr("disabled");
				} else{
					$("#avancar").attr("disabled", "disabled");
				}
			});

//verifica o preenchimento do cpf ao digitar
			$("#divDados input[type=text]").on("propertychange change keyup paste input", function(){
				if ( ($(this).val().length<14 && $(this).attr("id")=="iniCpf") || ($(this).val().length==0)){
					$("#avancar").attr("disabled", "disabled");
				} else{
					$(this).parent().next().find("label").removeClass("vis").addClass("nvis");
					$(this).removeClass("vazio");
					$("#avancar").removeAttr("disabled");
				}
				vazio=false;
				$("#divDados input[type=text]").each(function(i){
					if ($.trim($(this).val())==""){
						vazio=true;
						return;
					}
				});
				if (!vazio) {
					$("#avancar").removeAttr("disabled");
				} else{
					$("#avancar").attr("disabled", "disabled");
				}
			});
			
//Preenche o select 'selDisc' quando muda o curso
			$('#selCurso').change(function(){
				$('#selDisc').removeClass('vazio').addClass('borda');
				$('#selTurma').removeClass('vazio').addClass('borda');
				$('#lblRepetido').removeClass('vis').addClass('nvis');
				$.ajax({
					type: 'post',
					data: {curso: $('#selCurso').val(), page: 2},
					dataType: 'json',
					url: 'fillSel.php',
					success: function(dados){
						if (dados.length!=0){ //mostra as disciplinas no #selDisc
							$('#selDisc')[0].options.length = 0;
							$(dados).each(function(i){
								$('#selDisc').append($('<option>', {tipo: dados[i].tipo, cod_disc: dados[i].cod_disc, turma: dados[i].turma, value: dados[i].cod_turma, text: dados[i].cod_disc + dados[i].turma + " - " + dados[i].nome}));
							});
						} else { //Selecione uma disciplina
							$('#selDisc')[0].options.length = 0;
							$('#selDisc').append($('<option>', {value: 0, text: 'Selecione uma disciplina...'}));
						}
					},
					complete: function(dados){
						$tipoTurma = $('#selDisc option:selected').attr('tipo');
						$turma=$('#selDisc option:selected').attr('turma');
						$cod_disc=$('#selDisc option:selected').attr('cod_disc');
						if ($tipoTurma=='T'){
							$.ajax({
								type: 'post',
								data:{page: 3, turma: $turma, cod_disc: $cod_disc},
								dataType: 'json',
								url: 'fillSel.php',
								success: function(dadosSel){
									$('#selTurma')[0].options.length = 0;
									$(dadosSel).each(function(i){
										$('#selTurma').append($('<option>', {value: dadosSel[i].cod_turma, text: dadosSel[i].cod_disc + dadosSel[i].turma + " - " + dadosSel[i].nome}));
									});
								},
							})
						} else {
							$('#selTurma')[0].options.length = 0;
							$('#selTurma').append($('<option>', {value: 0, text: 'Disciplina sem turma prática'}));
						}
					}
				});
				if ($('#selDisc').val()==0 || $('#txtJustif').val()=="" || $('#selCurso').val()==0) {
					$("#salvar").attr("disabled", "disabled");
				} else {
					$("#salvar").removeAttr("disabled");
				};
			});

//Ativa ou Desativa o Avançar quando seleciona disciplina
			$('#selDisc').change(function(){
				$('#selDisc').removeClass('vazio').addClass('borda');
				$('#selTurma').removeClass('vazio').addClass('borda');
				$('#lblRepetido').removeClass('vis').addClass('nvis');
				$tipoTurma = $('#selDisc option:selected').attr('tipo');
				$turma=$('#selDisc option:selected').attr('turma');
				$cod_disc=$('#selDisc option:selected').attr('cod_disc');
				if ($tipoTurma=='T'){
					$.ajax({
						type: 'post',
						data:{page: 3, turma: $turma, cod_disc: $cod_disc},
						dataType: 'json',
						url: 'fillSel.php',
						success: function(dadosSel){
							$('#selTurma')[0].options.length = 0;
							$(dadosSel).each(function(i){
								$('#selTurma').append($('<option>', {value: dadosSel[i].cod_turma, text: dadosSel[i].cod_disc + dadosSel[i].turma + " - " + dadosSel[i].nome}));
							});
						},
					})
				} else {
					$('#selTurma')[0].options.length = 0;
					$('#selTurma').append($('<option>', {value: 0, text: 'Disciplina sem turma prática'}));
				}
				if ($('#selDisc').val()==0 || $('#txtJustif').val()=="" || $('#selCurso').val()==0) {
					$("#salvar").attr("disabled", "disabled");
				} else {
					$("#salvar").removeAttr("disabled");
				};
			});
			
//Seleciona turma			
			$('#selTurma').change(function(){
				$('#selDisc').removeClass('vazio').addClass('borda');
				$('#selTurma').removeClass('vazio').addClass('borda');
				$('#lblRepetido').removeClass('vis').addClass('nvis');
			});
			
// Ativa/desativa o salvar matrícula ao tirar o foco do textarea
			$("#txtJustif").blur(function(){
				if ($('#selDisc').val()==0 || $('#selCurso').val()==0 || $('#txtJustif').val()==""){
					$("#salvar").attr("disabled","disabled");
					if ($('#txtJustif').val()==""){
						$(this).next().removeClass("nvis").addClass("vis");
						$(this).addClass("vazio");
					} else {
						$(this).next().removeClass("vis").addClass("nvis");
						$(this).removeClass("vazio");
					}
				} else{
					$(this).next().removeClass("vis").addClass("nvis");
					$(this).removeClass("vazio");
					$("#salvar").removeAttr("disabled");
				}
			});

//Ativa/desativa o salvar matrícula ao digitar no textarea
			$("#txtJustif").on("propertychange change keyup paste input", function(){
				if ($('#selDisc').val()==0 || $('#selCurso').val()==0 || $('#txtJustif').val()==""){
					$("#salvar").attr("disabled","disabled");
					if ($('#txtJustif').val()==""){
						$(this).next().removeClass("nvis").addClass("vis");
						$(this).addClass("vazio");
					} else {
						$(this).next().removeClass("vis").addClass("nvis");
						$(this).removeClass("vazio");
					}
				} else{
					$(this).next().removeClass("vis").addClass("nvis");
					$(this).removeClass("vazio");
					$("#salvar").removeAttr("disabled");
				}
			});
			
//			
		});


</script>

<?php
	} else{ ?>
	<div id="foraPeriodo" class="width100">
		<h3>Fora do Período de Matrícula</h3>
		<b>Matrícula de Aluno Especial:</b> <i>de <?php echo(date("<\u>d/m/Y</\u> - H:i\h", strtotime($calend['iniAe'])))?> à <?php echo(date("<\u>d/m/Y</\u> - H:i\h;", strtotime($calend['fimAe'])))?></i><br>
		<b>Ajustes de Matrícula:</b> <i>de <?php echo(date("<\u>d/m/Y</\u> - H:i\h", strtotime($calend['iniAj'])))?> à <?php echo(date("<\u>d/m/Y</\u> - H:i\h;", strtotime($calend['fimAj'])))?></i><br>
		<b>Matrícula em disciplinas equivalentes:</b> <i>de <?php echo(date("<\u>d/m/Y</\u> - H:i\h", strtotime($calend['iniEq'])))?> à <?php echo(date("<\u>d/m/Y</\u> - H:i\h;", strtotime($calend['fimEq'])))?></i>
	</div>
	<?php } ?>
  </body>
</html>