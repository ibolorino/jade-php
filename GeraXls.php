<?php 
	require_once("conn.php");
	require_once("xlsxwriter.class.php");
	$filename=$_POST['txtFile'];
	$query=$_POST['txtQuery'];
	$result = mysqli_query($con, $query) or exit(); 
	if ($result!=false){
		header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Transfer-Encoding: binary');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');   
		$header = array();
		$field=mysqli_fetch_fields($result);
		foreach ($field as $val)
		{
			$header[$val->name]='string';
		}
		$writer = new XLSXWriter();
		$writer->writeSheetHeader('Sheet1', $header);
		$array = array();
		while ($row=mysqli_fetch_row($result)){
			for ($i=0; $i<mysqli_num_fields($result); $i++ ){
				$array[$i] = $row[$i];
			}
				$writer->writeSheetRow('Sheet1', $array);
		};
		$writer->writeToStdOut();
	} else{
		header("location:javascript://history.go(-1)");
	}
	exit(0);
?>