<?php 
	session_start(); 
	date_default_timezone_set('America/Sao_Paulo');
?>
<html lang="pt-br">
	<head>
		<title>Matrícula de Aluno Especial - FCAV - 2018</title>
		<script src="jquery-3.3.1.js" type="text/javascript"></script>
		<script src="jquery.mask.js" type="text/javascript"></script>
		<link rel="stylesheet" type="text/css" href="estilo.css">
		<link rel="shortcut icon" href="">
		<meta charset="utf-8">
	</head>
	<body>
	
	
	<?php
	$hoje = strtotime(date('Y/m/d H:i',time())); 
	?>
	
		<div id="divFunc" class="vis">
				Título: <br><input type="text" id="txtTitulo" style="width:100%;" maxlength="250">
				<p>Descrição:
				<textarea id="txtDescr" rows="5" maxlength="255" style="resize:none; width: 100%;" id="txtJustif"></textarea><p>
				<input type="button" id="btSalvar" value="Salvar">
		</div>
	
		
		<script>
			
			$('#btSalvar').click(function(){
				$.ajax({
					type: 'post',
					data: {titulo: $('#txtTitulo').val(), descr: $('#txtDescr').val()},
					dataType: 'json',
					url: 'svFunc.php',
					complete: function(){
						$('#txtDescr').val('');
						$('#txtTitulo').val('');
						$('#txtTitulo').focus();
					},
				});
			});

		</script>
	</body>
</html>