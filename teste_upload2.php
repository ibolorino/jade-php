<html>
	<head lang="en">
		<title>Teste Upload</title>
		<script src="jquery-3.3.1.js" type="text/javascript"></script>
		<script src="jquery.mask.js" type="text/javascript"></script>
		<link rel="stylesheet" type="text/css" href="estilo.css">
		<link rel="shortcut icon" href="">
		<meta charset="utf-8">
	</head>
	<body>
		<form id="frmUpload" action='' method="post" enctype="multipart/form-data">
			<label for='upFile' id='lbFile'>Selecione um arquivo</label>
			<input id="upFile" type="file"><p>
			<input id='btFile' class="btPequeno" type="button" value="Upload">
		</form>
	</body>
</html>