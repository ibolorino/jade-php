<html>
<head>
<script src="jquery-3.3.1.js" type="text/javascript"></script>
	<script src="jquery.mask.js" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="estilo.css">
</head>
<body>

	<div class="width100">
		<table style='border-collapse: collapse;' frame="box" width="100%">
			<tr height='20'>
				<td rowspan='1'colspan='6' align ='left' valign ='bottom' style='font-weight: bold;font-style: italic;text-decoration: underline;color: rgb(0,0,0);'>Parâmetros da pesquisa:</td>
			</tr>
			<tr height='20'>
				<td  valign ='bottom' style='color: rgb(0,0,0);' colspan="6"></td>
			</tr>
			<tr height='20'>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'><input type='text' id='aluno' style="width: 100%;"></td>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'><input type='text' id='curso_aluno' style="width: 100%;"></td>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'><input type='text' id='disc' style="width: 100%;"></td>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'><input type='text' id='depto_disc' style="width: 100%;"></td>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'><input type='text' id='curso_disc' style="width: 100%;"></td>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'><input type='text' id='tipo' style="width: 100%;"></td>
			</tr>
			<tr height='20'>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'>Aluno</td>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'>Curso do aluno</td>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'>Disciplina</td>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'>Departamento</td>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'>Curso da disciplina</td>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);'>Tipo de Matrícula</td>
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="6"><b id="qryMan">Query manual</b><input type='button' id='btQryMan' value='Gerar Arquivo' class='btPequeno' hidden="hidden"></td>
			</tr>
			<tr>
				<td  align ='center' valign ='middle' style='color: rgb(0,0,0);' colspan="6"><textarea id='txtQryMan' style="width: 100%;" rows="5" class="nvis"></textarea></td>
			</tr>
			<tr>
				<td colspan="6">&nbsp;</td>
			</tr>
		</table>
	</div>
	<div class="linhaestreito">
		<input type="button" id="btBuscar" value="Pesquisar">
	</div>
	<div id="divExport" class="linhaestreito">
		<input type="button" id="btExptAluno" value="Exportar Alunos" class="btPequeno" style="width: 130px;"><br>
		<input type="button" id="btExptDisc" value="Exportar Disciplinas" class="btPequeno" style="width: 130px;"><br>
		<input type="button" id="btExptMatr" value="Exportar Matrículas" class="btPequeno" style="width: 130px;">
	</div>
	<div id="divBt" class="nvis">
		<select id="selLote">
			<option value="Deferido">Deferido</option>
			<option value="Em análise" selected>Em análise</option>
			<option value="Indeferido">Indeferido</option>
		</select>
		&nbsp;
		<input type="button" id="btChgStatus" value="Alterar todos os status" class="btPequeno">
	</div>
	<div id="divNotFound" class="nvis">
		<h3>Nenhuma solicitação de matrícula encontrada.</h3>
	</div>
	
	<div id="divXls" class="nvis">
		<form id="frmXls" method="post">
			<input type="text" id="txtFile" name="txtFile">
			<input type="text" id="txtQuery" name="txtQuery">
			<input type="text" id="txtHeader" name="txtHeader">
		</form>
	</div>
	
	<div id="divShowDados" class="nvis width100">
		<table id="tblDados" width= "100%" style="border-collapse: collapse;">
			<tr height="20">
			<td colspan="3" align="left">Matrículas:</td>
			<td id="nReg" colspan="4" align="right"></td>
			</tr>
			<tr><td colspan="6"></td></tr><tr><td colspan="6"></td></tr><tr><td colspan="6"></td></tr>
			<tr height="20" id="trDados">
				<td valign ="top" style="font-weight: bold;color: rgb(0,0,0);border-bottom: solid 1px black;border-top: solid 1px black;border-left: solid 1px black;border-right: solid 1px black;">Cod_Matrícula</td>
				<td valign ="top" style="font-weight: bold;color: rgb(0,0,0);border-bottom: solid 1px black;border-top: solid 1px black;border-left: solid 1px black;border-right: solid 1px black;">Aluno</td>
				<td valign ="top" style="font-weight: bold;color: rgb(0,0,0);border-bottom: solid 1px black;border-top: solid 1px black;border-left: solid 1px black;border-right: solid 1px black;">Curso do Aluno</td>
				<td valign ="top" style="font-weight: bold;color: rgb(0,0,0);border-bottom: solid 1px black;border-top: solid 1px black;border-left: solid 1px black;border-right: solid 1px black;">Tipo de Matrícula</td>
				<td valign ="top" style="font-weight: bold;color: rgb(0,0,0);border-bottom: solid 1px black;border-top: solid 1px black;border-left: solid 1px black;border-right: solid 1px black;">Disciplina</td>
				<td valign ="top" style="font-weight: bold;color: rgb(0,0,0);border-bottom: solid 1px black;border-top: solid 1px black;border-left: solid 1px black;border-right: solid 1px black;">Curso da Disciplina</td>
				<td valign ="top" style="font-weight: bold;color: rgb(0,0,0);border-bottom: solid 1px black;border-top: solid 1px black;border-left: solid 1px black;border-right: solid 1px black;">Status</td>
			</tr>
		</table>
	</div>
	<p>
	

	

	
	<script>
	$("#btExptAluno").click(function(){
		$("#txtQuery").val('select * from aluno;');
		$("#frmXls").attr('action','Testexls.php');
		$("#frmXls").submit();
	});
	
	$("#btExptDisc").click(function(){
		$("#txtQuery").val('select * from disciplina;');
		$("#frmXls").attr('action','Testexls.php');
		$("#frmXls").submit();
	});
	
	$("#btExptMatr").click(function(){
		$("#txtQuery").val('select * from matricula;');
		$("#frmXls").attr('action','Testexls.php');
		$("#frmXls").submit();
	});
	//Clique em Gerar Query Manual
	$("#btQryMan").click(function(){
		$("#txtQuery").val($("#txtQryMan").val());
		$("#frmXls").attr('action','Testexls.php');
		$("#frmXls").submit();
	});
	
	//Clique em Query manual
	$("#qryMan").click(function(){
		if ($("#btQryMan").attr("hidden")){
			$("#btQryMan").removeAttr("hidden");
			$("#txtQryMan").removeClass("nvis").addClass("vis");
		} else {
			$("#btQryMan").attr("hidden","hidden");
			$("#txtQryMan").removeClass("vis").addClass("nvis");
		}
	});
	
	
	
	//Clique no botão Alterar em lote
	$("#btChgStatus").click(function(){
		$.ajax({
			type:'post',
			data:{page: 4, status: $("#selLote").val()},
			dataType: 'json',
			url: 'check_consultas.php',
			success: function(dados){
				$("select.unico").val($("#selLote").val()).change();
			},
		});
	});
	
	
	//Clique no botão buscar
	$("#btBuscar").click(function() {
		$.ajax({
			type:'post',
			data: {aluno: $('#aluno').val(), curso_aluno: $('#curso_aluno').val(), disc: $('#disc').val(), depto_disc: $('#depto_disc').val(), curso_disc: $('#curso_disc').val(), tipo: $('#tipo').val(), page: 1},
			//page = 1: pesquisar matriculas
			dataType: 'json',
			url: 'check_consultas.php',
			success: function(dados){
				if (dados.length>=1){
					$("tr.remover").each(function(i){
						$(this).remove();
					});
					$("#nReg").empty();
					$("#nReg").append(dados.length + " solicitações de matrícula encontradas. <input type='button' id='btTsv' value='Exportar' class='btPequeno'>");
					$currentTr=$("#trDados");
					$(dados).each(function(i){
						if (i>=0){
							var $inserir='<tr class="remover"><td class="borda" id="tdCod">'+dados[i].cod_matr+'</td><td class="borda">'+dados[i].aluno+'</td><td class="borda">'+dados[i].curso_aluno+'</td><td class="borda">'+dados[i].tipo+'</td><td class="borda">'+dados[i].disc+'</td><td class="borda">'+dados[i].curso_disc+'</td><td class="borda">';
							
							
							//$('<tr class="remover"><td class="borda">'+dados[i].cod_matr+'</td><td class="borda">'+dados[i].aluno+'</td><td class="borda">'+dados[i].curso_aluno+'</td><td class="borda">'+dados[i].tipo+'</td><td class="borda">'+dados[i].disc+'</td><td class="borda">'+dados[i].curso_disc+'</td><td class="borda">'+dados[i].status+'</td></tr>').insertAfter($currentTr);
							
							if (dados[i].status == "Em análise") {
								$inserir += '<select class="unico"><option value="Deferido">Deferido</option><option value="Em análise" selected>Em análise</option><option value="Indeferido">Indeferido</option></select>';
							} else if (dados[i].status == "Deferido") {
								$inserir += '<select class="unico"><option value="Deferido" selected>Deferido</option><option value="Em análise">Em análise</option><option value="Indeferido">Indeferido</option></select>';
							} else if (dados[i].status == "Indeferido") {
								$inserir += '<select class="unico"><option value="Deferido">Deferido</option><option value="Em análise">Em análise</option><option value="Indeferido" selected>Indeferido</option></select>';
							} else { //status com erro
								$inserir += dados[i].status+'</td>';
							}
							$inserir += '<br><div class="nvis alter">Salvo!</div></td></tr>';
							$($inserir).insertAfter($currentTr);
							
							$currentTr=$currentTr.next();
						}
					});
					$("#divShowDados").removeClass("nvis").addClass("vis");
					$("#divNotFound").removeClass("vis").addClass("nvis");
					$("#divBt").removeClass("nvis").addClass("vis linhalargo");
				} else { //nao encontrou
					$("#divNotFound").removeClass("nvis").addClass("vis");
					$("#divShowDados").removeClass("vis").addClass("nvis");
					$("#divBt").removeClass("vis linhalargo").addClass("nvis");
				}
			},
			complete: function(){
				
				$('select.unico').change(function(){
					var $cel = $(this).parent();
					var $cod_matr=$(this).parent().parent().find("td:first").html();
					var $status=$(this).val();
					$.ajax({
						type:'post',
						data: {cod_matr: $cod_matr, status: $status, page: 3},
						//page = 3: altera status
						dataType: 'json',
						url: 'check_consultas.php',
						success: function(dados){
							$cel.find("div").removeClass("nvis").addClass("vis");
						},
					});
				});

				$("#btTsv").click(function(){
					$.ajax({
						type:'post',
						data: {page: 6, where: window.where},
						dataType: 'json',
						url: 'check_consultas.php',
						success: function(dados){
							$("#txtFile").val(dados[0]);
							$("#txtQuery").val(dados[1]);
							$("#txtHeader").val("sim");
							$("#frmXls").attr('action','GeraXls.php');
							$("#frmXls").submit();
							
						},
					});
				});
				
			}
		});
	});
	
//alteração de status de matrícula
	
	
	</script>
	
	
	</body>
	</html>