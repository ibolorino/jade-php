function loadSelect($selector, $objeto, callback, $firstOption){ //a partir de um objeto do BD, retorna value e text e preenche um <select>, executando uma função callback após
		$.ajax({
			type: 'post',
			data: {page: 23, objeto: $objeto, firstOption: $firstOption},
			dataType: 'json',
			url: 'check.php',
			success: function(dtSelector){
				//console.log(dtSelector);
				if (dtSelector.length!=0){
					$selector[0].options.length = 0;
					if ($firstOption) $selector.append($('<option>', {value: 0, text: $firstOption}));
					$(dtSelector).each(function(i){
						$selector.append($('<option>', {value: Object.values(dtSelector[i])[0], text: Object.values(dtSelector[i])[1]}));
					});
				} else {
					$selector[0].options.length = 0;
					$selector.append($('<option>', {value: Object.values(dtSelector[0])[0], text: Object.values(dtSelector[0])[1]}));
				}
			},
			complete: function(){
				if (callback) callback();
				return;
			}
		});
	}

function queryFromTable($tableBD, $fieldsBD, $tableSite, $header, $primaryKeyAutoIncrement, $valueExtra){
	$valueExtra=($valueExtra != '')?',' + $valueExtra:$valueExtra;
	$nLinhas=0;
	$tableSite.find('tr').each(function(i){
		$nLinhas+=1;
	});
	$nColunas=0;
	$tableSite.find('tr:first td').each(function(i){
		$nColunas+=1;
	});
	$autoIncrement=($primaryKeyAutoIncrement==true)?'0,':'';
	$fields='';
	$updateFields='';
	//console.log("tamanho: " + $fieldsBD[1]);
	$fields = $fieldsBD[0] + ',';
	for (var iFunc = 1; iFunc <= $fieldsBD.length - 1; iFunc++){
		$fields += $fieldsBD[iFunc] + ',';
		$updateFields += $fieldsBD[iFunc] + ' = VALUES(' + $fieldsBD[iFunc] + '), '; 
	}
	$fields = '(' + $fields.substr(0,$fields.length-1) + ')';
	$updateFields = $updateFields.substr(0,$updateFields.length-2);
	$query="INSERT INTO " + $tableBD + " " + $fields + " VALUES ";
	if ($header==true){
		for (var iLin=1;iLin <= $nLinhas;iLin++){
			$query += "(" + $autoIncrement;
			for (var iCol=1;iCol <= $nColunas;iCol++){
				$query += '"' + $tableSite.find('tr:nth-child(' + iLin + ')').find('td:nth-child(' + iCol + ')').html() + '",'; 
			}
			$query = $query.substr(0,$query.length-1);
			$query += "),";
		}
	} else{
		for (var iLin=2;iLin <= $nLinhas;iLin++){
			$query += "(" + $autoIncrement;
			for (var iCol=1;iCol <= $nColunas;iCol++){
				$query += '"' + $tableSite.find('tr:nth-child(' + iLin + ')').find('td:nth-child(' + iCol + ')').html() + '",'; 
			}
			$query = $query.substr(0,$query.length-1);
			$query += $valueExtra + "),";
		}
	}
	$query = $query.substr(0,$query.length-1);
	$query += " ON DUPLICATE KEY UPDATE " + $updateFields + ";";
	return $query;
}


function clearDiv($div){	
	$($div).find('input[type=text]').each(function(iFunc){
		$(this).removeClass('vazio');
		$(this).val('');
	});
	$($div).find('select').each(function(iFunc){
		$(this).removeClass('vazio');
		$(this)[0].options.length=0;
		$(this).append( $('<option>', {value: 0, text: 'Selecione ' + $(this).attr('name') + '...'}) );
		$(this).val(0);
	});
	$($div).find('label.alter').each(function(iFunc){
		$(this).removeClass('vis').addClass('nvis');
	});
}


function exibeDadosTable(dataAjax, colIni, colFim, classesTrHeader, classesTrDados){
	//a partir de uma requisição ajax que retorna header e dados, gera o html de uma tabela (tr e td) para exibição
	classesTrHeader = ' ' + classesTrHeader;
	classesTrDados = ' ' + classesTrDados;
	$htmlTable="<tr class='remover" + classesTrHeader + "'>";
	for (var iFunc=colIni;iFunc<colFim;iFunc++){
		header=Object.values(dataAjax[0])[iFunc];
		$htmlTable+="<td align='center' style='font-weight: bold;' class='bordaVertBlack'>" + header + "</td>";
	}
	$htmlTable+="</tr>";
	for (var iFunc=1;iFunc<Object.values(dataAjax).length;iFunc++){
		$htmlTable+="<tr class='remover" + classesTrDados + "'>";
		for (var jFunc=colIni;jFunc<colFim;jFunc++){
			$htmlTable+="<td class='bordaVertBlack'>" + Object.values(dataAjax[iFunc])[jFunc] + "</td>";
		}
		$htmlTable+="</tr>";
	}
	return $htmlTable;
}

function geraTableEdit(dataAjax, id, colIni, colFim, classesTrHeader, classesTrDados){
	$htmlTable="";
	for (var iFunc=colIni;iFunc<colFim;iFunc++){
		$htmlTable+="<tr class='remover " + classesTrHeader + "'>";
		$htmlTable+="<td align='right'>" + Object.values(dataAjax[0])[iFunc] + ": </td>";
		$htmlTable+="<td align='center'><input type='text' id='" + id + "' class='borda' value='" + Object.values(dataAjax[1])[iFunc] + "'></input></td>";
		$htmlTable+="</tr>";
	}
	return $htmlTable;
}


function dateHorario($data){ //a princípio, converte data no formato "YYYY-mm-dd hh:ii:ss" para "hh:mm"
			// Posteriormente, identificar formatos de entrada e saída e retornar adequadamente
	if ($data.length==19){
		return $data.substr(11,2) + ':' + $data.substr(14,2);
	} else {
		return $data;
	}
}

function dateDia($data){ //a princípio, converte data no formato "YYYY-mm-dd hh:ii:ss" para "dd/mm/YYYY"
			// Posteriormente, identificar formatos de entrada e saída e retornar adequadamente
	if ($data.length==19){
		return $data.substr(8,2) + '\/' + $data.substr(5,2) + '\/' + $data.substr(0,4);
	} else {
		return $data;
	}
}


function convertDate($data, $tipo){ //converte a data de um formato para outro, conforme o tipo

	if ($tipo == 1){ // "dd/mm/YYYY hh:ii" para "YYYY-mm-dd hh:ii:00"
		return $data.length==16?$data.substr(6,4) + '-' + $data.substr(3,2) + '-' + $data.substr(0,2) + $data.substr(10,6) + ':00':$data.substr(6,4) + '-' + $data.substr(3,2) + '-' + $data.substr(0,2) + ' 00:00:00';
		
	} else if ($tipo == 2){ // "YYYY-mm-dd hh:ii:00" para "dd/mm/YYYY hh:ii"
		return $data.length==19?$data.substr(8,2) + '\/' + $data.substr(5,2) + '\/' + $data.substr(0,4) + $data.substr(10,6):$data.substr(8,2) + '\/' + $data.substr(5,2) + '\/' + $data.substr(0,4) + ' 00:00';
		
	}
}


function isDate(txtDate) {
	var currVal = txtDate;
	if(currVal == '') return false;
	//Declare Regex 
	var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
	var dtArray = currVal.match(rxDatePattern); // is format OK?
	if (dtArray == null) return false;
	//Checks for mm/dd/yyyy format.
	dtMonth = dtArray[3];
	dtDay= dtArray[1];
	dtYear = dtArray[5];
	if (dtMonth < 1 || dtMonth > 12) return false;
	else if (dtDay < 1 || dtDay> 31) return false;
	else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31) return false;
	else if (dtMonth == 2) {
		var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
		if (dtDay> 29 || (dtDay ==29 && !isleap)) return false;
	}
	return true;
}

function isHour(txtHour) { //verifica se time é valido no padrão H:ii:ss; H{0~23}, ii{00~59}, ss{00~59}
	var currVal = txtHour;
	if(currVal == '') return false;
	rxHourPattern=currVal.length==5?/^(\d{2})(:)(\d{2})$/:/^(\d{2})(:)(\d{2})(:)(\d{2})$/;
	hrArray=currVal.match(rxHourPattern);
	if (hrArray==null) return false;
	if (hrArray[1] < 0 || hrArray[1] > 23) return false;
	if (hrArray[3] < 0 || hrArray[3] > 59) return false;
	if ((hrArray[5] < 0 || hrArray[5] > 59) && currVal.length==8) return false;
	return true;
}


function addColuna(tabela, tag, prop, header, n){
	//inclui uma coluna na tabela onde tag é o elemento, prop são as propriedades, header é o título da coluna e n é a posição da coluna na tabela
	if (n<1 || n=='first'){
		n=1;
	} else if (n>$(tabela).find('tr:first td').length){
		n='last';
	} else if (n>=1 && n<= $(tabela).find('tr:first td').length) {
		n=n;
	} else {
		n='last';
	}
	$(tabela).find('tr').each(function(j){
		if (n != 'last'){
			$cell=$(this).find('td:nth-child(' + n + ')');
			$(this).find('td:nth-child(' + n + ')').clone().insertAfter($cell);
		} else{
			$cell=$(this).find('td:last');
			$(this).find('td:last').clone().insertAfter($cell);
			$cell=$(this).find('td:last');
		}
		$cell.attr('align','center');

		$cell.html('');
		elementImg='';
		for (iFunc=0;iFunc<=prop.length-1;iFunc++){
			innerImg='';
			elementImg += "<" + tag + " ";
			for (var key in prop[iFunc]){
				innerImg += key + "='" + prop[iFunc][key] + "' ";
			}
			innerImg=innerImg.substr(innerImg,innerImg.length-1);
			elementImg += innerImg + '>&nbsp;';
		}
		elementImg=elementImg.substr(0,elementImg.length-6);
		$cell.append(elementImg);
	});
	if (n!='last'){
		$(tabela).find('tr:first').find('td:nth-child(' + n + ')').html(header);
	} else {
		$(tabela).find('tr:first').find('td:last').html(header);
	}
}



function addColumnTable(tabela, icones, header, n){
	//inclui uma coluna na tabela onde icones é um objeto key-value com os atributos <img>, header é o título da coluna e n é a posição da coluna na tabela
	if (n<1 || n=='first'){
		n=1;
	} else if (n>$(tabela).find('tr:first td').length){
		n='last';
	} else if (n>=1 && n<= $(tabela).find('tr:first td').length) {
		n=n;
	} else {
		n='last';
	}
	$(tabela).find('tr').each(function(j){
		if (n != 'last'){
			$cell=$(this).find('td:nth-child(' + n + ')');
			$(this).find('td:nth-child(' + n + ')').clone().insertAfter($cell);
		} else{
			$cell=$(this).find('td:last');
			$(this).find('td:last').clone().insertAfter($cell);
			$cell=$(this).find('td:last');
		}
		$cell.attr('align','center');

		$cell.html('');
		elementImg='';
		for (iFunc=0;iFunc<=icones.length-1;iFunc++){
			innerImg='';
			elementImg += "<img ";
			for (var key in icones[iFunc]){
				innerImg += key + "='" + icones[iFunc][key] + "' ";
			}
			innerImg=innerImg.substr(innerImg,innerImg.length-1);
			elementImg += innerImg + '>&nbsp;';
		}
		elementImg=elementImg.substr(0,elementImg.length-6);
		$cell.append(elementImg);
	});
	if (n!='last'){
		$(tabela).find('tr:first').find('td:nth-child(' + n + ')').html(header);
	} else {
		$(tabela).find('tr:first').find('td:last').html(header);
	}
}