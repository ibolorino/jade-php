<html lang="pt-br">
  <head>
    <title>Matrícula de Aluno Especial - FCAV - 2018</title>
    <meta charset="utf-8">
	<script src="../jquery-3.3.1.js" type="text/javascript"></script>
	<script src="../jquery.mask.js" type="text/javascript"></script>
	<script src="functions.js" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="estilo.css">
  </head>
  <body>
	<input type='button' id='btTeste' class='nvis' value='Teste'>
	<div id="divConsInicio" class="vis">
		<form id="frmInicio" method="post" action="fim.php">
		<h4>Entre com o CPF:</h4>
				<table style="border-collapse: collapse;">
					<tr height="20">
						<td><input maxlength="14" type="text" id="iniCpf" name="iniCpf" class="borda"></td><td><label class="nvis"> Campo obrigatório</label></td>
					</tr>
					<tr height="20">
						<td><br><input type="button" id="btBuscar" value="Pesquisar"></td>
					</tr>
				</table>
						<p>
						
		</form>		
	</div>
	<div id="divNotFound" class="nvis">
		<h3>Nenhuma solicitação de matrícula encontrada.</h3>
		<h4>Verifique o CPF digitado ou entre em contato com a Seção de Graduação.</h4>
	</div>
	<div id="divConsDados" class="nvis width100">
		<table width= "100%" style='border-collapse: collapse;'>
			<h3 align="center">Solicitações de matrícula:</h3>
			<tr height="40">
				<td>
				<table width="100%" style="border-collapse: collapse;">
					<tr>
						<td id="nReg" align="right" width="100%"></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td>
					<table width= "100%" style='border-collapse: collapse; border: 2px solid black; font-size: 14;'>
						<tr id="trDados" height="20" class="zebraAzulTitulo">
							<td valign ="top" class="borda" style="font-weight: bold;">Nome</td>
							<td valign ="top" class="borda" style="font-weight: bold;">Tipo de Matrícula</td>
							<td valign ="top" class="borda" style="font-weight: bold;">Disciplina</td>
							<td valign ="top" class="borda" style="font-weight: bold;">Curso da Disciplina</td>
							<td valign ="top" class="borda" style="font-weight: bold;">Status</td>
							<td valign ="top" class="borda" style="font-weight: bold;" width="30%">Parecer</td>
							<td valign ="top" class="borda" style="font-weight: bold;">Excluir</td>
						</tr>
					</table>
				<td>
			</tr>
		</table>
	</div>
	<table id="tblDados" style="border-collapse: collapse;" frame="box" width="100%">
			
	</table>
	<p>
	<span style="background-color: #dfdfdf; float: left; width: 100%;"><b>ATENÇÃO: </b>O prazo para análise dos docentes é até dia <span id='spanPrazo'>26/07/2019</span>. Caso ainda não tenha resposta após esta data, entre em contato direto com o Docente responsável pela disciplina.</span>
<script>

	$('#btBuscar').click(function(){
		$divNotFound=$('#divNotFound');
		$divNotFound.removeClass('vis').addClass('nvis');
		$tabela=$('#tblDados');
		$tabela.find('tr.remover').remove();
		$.ajax({
			type: 'post',
			data: {cpf: $('#iniCpf').val(), page: 4},
			dataType: 'json',
			url: 'check.php',
			success: function(dtDisciplinas){
				//console.log(dtTeste);
				if (dtDisciplinas.length > 1){
					$tabela.append(exibeDadosTable(dtDisciplinas,2,Object.values(dtDisciplinas[0]).length,'zebraAzulTitulo',"zebraAzul"));
					icones=[];
					icones.push({'src':'imgs/del_lixeira.png', 'height':'18', 'width':'18', 'class':'imgLink'});
					addColuna($('#tblDados'),'img', icones,'Excluir','last');
					for ( var i = 1; i < dtDisciplinas.length; i++){
						if (dtDisciplinas[i].atendida=='sim'){ //excluir botão
							var j=i+1;
							$tabela.find('tr:nth-child('+j+')').find('td:last').html('Não é possível excluir.');
						}
					}
				} else { //nenhuma matricula encontrada
					$divNotFound.removeClass('nvis').addClass('vis');
				}
				
			},
			complete: function(dtDisciplinas){
				dtDisciplinas=dtDisciplinas.responseJSON;
				$(document).on('click','img', function(){
					if (confirm('Tem certeza que deseja excluir a solicitação?')){
						var $tr = $(this).closest('tr');
						var $linha = $tr.index();
						$cod_matr=dtDisciplinas[$linha].cod_matr;
						$tr.remove();
						$.ajax({
							type: 'post',
							data: {cod_matr: $cod_matr, page: 5},
							dataType: 'json',
							url: 'check.php',
							complete: function(dados){
								$('#btBuscar').trigger('click');
							},
						});
					}
				});
			},
		});
	});

	
	
	$(document).ready(function () { 
	
//mascara do cpf	
       $("#iniCpf").mask('000.000.000-00', {reverse: true});

//deixa o botão 'avançar' habilitado se o cpf estiver preenchido	   
	   $(function(){
			if ($("#iniCpf").val()==""){
				$("#btBuscar").attr("disabled", "disabled");
			} else{
				$("#btBuscar").removeAttr("disabled");
			}
		});

//verifica o preenchimento do cpf ao sair do foco			
		$("#iniCpf").blur(function(){
			if ($(this).val()==""){
				$(this).parent().next().find("label").removeClass("nvis").addClass("vis");
				$(this).addClass("vazio");
				$("#btBuscar").attr("disabled", "disabled");
			} else{
				$(this).parent().next().find("label").removeClass("vis").addClass("nvis");
				$(this).removeClass("vazio");
				$("#btBuscar").removeAttr("disabled");
			}
		});
		
//verifica o preenchimento do cpf ao digitar
		$("#iniCpf").on("propertychange change keyup paste input", function(){
			if ($(this).val().length<14){
				$("#btBuscar").attr("disabled", "disabled");
			} else{
				$(this).parent().next().find("label").removeClass("vis").addClass("nvis");
				$(this).removeClass("vazio");
				$("#btBuscar").removeAttr("disabled");
			}
		});	
	});
	
	
	
//Clique no botão buscar
/*
	$("#btBuscar").click(function() {
		$.ajax({
			type:'post',
			data: {cpf: $('#iniCpf').val(), page: 4},
			dataType: 'json',
			url: 'check.php',
			success: function(dados){
				console.log(dados);
				if (dados.length>=1){
					$("tr.remover").each(function(i){
						$(this).remove();
					});
					$("#nReg").empty();
					$("#nReg").append(dados.length-1 + " solicitações de matrícula encontradas.");
					$currentTr=$("#trDados");
					$(dados).each(function(i){
						if (i>0){
							$inserir='<tr class="remover zebraAzul"><td class="borda">'+dados[i].Aluno+'</td><td class="borda">'+dados[i].Tipo+'</td><td class="borda">'+dados[i].Disciplina+'</td><td class="borda">'+dados[i].Curso+'</td><td class="borda">'+dados[i].Status+'</td><td class="borda">'+dados[i].Parecer+'</td><td class="borda" align="center">';
							if (dados[i].status=='Em análise'){
								$inserir+='<div class="divHover"><img src="imgs/del_lixeira.png" height="18" width="18"><label class="nvis">'+dados[i].cod_matr+'</label></div>';
							} else {
								$inserir+='Não é possível excluir';
							}
							$inserir+='</td></tr>';
							$($inserir).insertAfter($currentTr);
							$currentTr=$currentTr.next();
						}
					});
					$("#divConsDados").removeClass("nvis").addClass("vis");
					$("#divNotFound").removeClass("vis").addClass("nvis");
				} else { //nao encontrou
					$("#divNotFound").removeClass("nvis").addClass("vis");
					$("#divConsDados").removeClass("vis").addClass("nvis");
				}
			},
		});
	});



*/


































/*
	$("#btBuscar").click(function() {
		$.ajax({
			type:'post',
			data: {cpf: $('#iniCpf').val(), page: 4},
			dataType: 'json',
			url: 'check.php',
			success: function(dados){
				if (dados.length>=1){
					$("tr.remover").each(function(i){
						$(this).remove();
					});
					$("#nReg").empty();
					$("#nReg").append(dados.length + " solicitações de matrícula encontradas.");
					$currentTr=$("#trDados");
					$(dados).each(function(i){
						if (i>=0){
							$inserir='<tr class="remover zebraAzul"><td class="borda">'+dados[i].aluno+'</td><td class="borda">'+dados[i].tipo+'</td><td class="borda">'+dados[i].disciplina+'</td><td class="borda">'+dados[i].curso+'</td><td class="borda">'+dados[i].status+'</td><td class="borda">'+dados[i].parecer+'</td><td class="borda" align="center">';
							if (dados[i].status=='Em análise'){
								$inserir+='<div class="divHover"><img src="imgs/del_lixeira.png" height="18" width="18"><label class="nvis">'+dados[i].cod_matr+'</label></div>';
							} else {
								$inserir+='Não é possível excluir';
							}
							$inserir+='</td></tr>';
							$($inserir).insertAfter($currentTr);
							$currentTr=$currentTr.next();
						}
					});
					$("#divConsDados").removeClass("nvis").addClass("vis");
					$("#divNotFound").removeClass("vis").addClass("nvis");
				} else { //nao encontrou
					$("#divNotFound").removeClass("nvis").addClass("vis");
					$("#divConsDados").removeClass("vis").addClass("nvis");
				}
			},
			
			
			/*
			complete: function(){
				$(document).on('click','img', function(){
				
					if (confirm('Tem certeza que deseja excluir a solicitação?')){
						
						$cod_matr=$(this).parent().find('label').html();
						$(this).parent().parent().parent().remove();
						
						
						
						
						
						
						
						$.ajax({
							type: 'post',
							data: {cod_matr: $cod_matr, page: 5},
							dataType: 'json',
							url: 'check.php',
							complete: function(dados){
								//console.log(dados);
								
								$('#btBuscar').trigger('click');
								
								/*
								if (dados.length>=1){
									$("tr.remover").each(function(i){
										$(this).remove();
									});
									
									
									$("#nReg").empty();
									$("#nReg").append(dados.length + " solicitações de matrícula encontradas.");
									
									$currentTr=$("#trDados");
									$(dados).each(function(i){
										if (i>=0){
											$inserir='<tr class="remover zebraAzul"><td class="borda">'+dados[i].aluno+'</td><td class="borda">'+dados[i].tipo+'</td><td class="borda">'+dados[i].disciplina+'</td><td class="borda">'+dados[i].curso+'</td><td class="borda">'+dados[i].status+'</td><td class="borda">'+dados[i].parecer+'</td><td class="borda" align="center">';
											if (dados[i].status=='Em análise'){
												$inserir+='<div class="divHover"><img src="imgs/del_lixeira.png" height="18" width="18"><label class="nvis">'+dados[i].cod_matr+'</label></div>';
											} else {
												$inserir+='Não é possível excluir';
											}
											$inserir+='</td></tr>';
											$($inserir).insertAfter($currentTr);
											$currentTr=$currentTr.next();
										}
									});
									
									$("#divConsDados").removeClass("nvis").addClass("vis");
									$("#divNotFound").removeClass("vis").addClass("nvis");
								} else { //nao encontrou
									$("#divNotFound").removeClass("nvis").addClass("vis");
									$("#divConsDados").removeClass("vis").addClass("nvis");
								}
								
								
							},
							*/




</script>
  </body>
</html>